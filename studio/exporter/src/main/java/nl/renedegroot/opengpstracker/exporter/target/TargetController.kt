/*
 * Open GPS Tracker
 * Copyright (C) 2019 René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses>.
 */
package nl.renedegroot.opengpstracker.exporter.target

import android.app.Activity.RESULT_OK
import android.content.ContentResolver
import android.content.Intent
import android.net.Uri
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.drive.Drive
import nl.renedegroot.opengpstracker.exporter.ExportActivity

private const val REQUEST_CODE_SIGN_IN = 110

private const val REQUEST_EXTERNAL_ACCESS = 12344

/**
 * Communicates with the Google Drive
 */
internal class TargetController {

    fun requestDriveConnection(activity: ExportActivity, onConnected: (Connection) -> Unit) {
        val signInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(Drive.SCOPE_FILE)
                .build()
        val client = GoogleSignIn.getClient(activity, signInOptions)
        activity.registerActivityResult(REQUEST_CODE_SIGN_IN) { resultCode, _ ->
            if (resultCode == RESULT_OK) {
                val signInAccount = GoogleSignIn.getLastSignedInAccount(activity)
                if (signInAccount != null) {
                    onConnected(Connection.DriveSuccess(Drive.getDriveResourceClient(activity, signInAccount)))
                } else {
                    onConnected(Connection.DriveFailure)
                }
            } else {
                onConnected(Connection.DriveFailure)
            }
        }
        activity.startActivityForResult(client.signInIntent, REQUEST_CODE_SIGN_IN)
    }

    fun checkDriveConnection(activity: ExportActivity, onConnected: (Connection) -> Unit) {
        val signInAccount = GoogleSignIn.getLastSignedInAccount(activity)
        val existingConnection = if (signInAccount != null) {
            Drive.getDriveResourceClient(activity, signInAccount)
        } else {
            null
        }
        if (existingConnection != null) {
            onConnected(Connection.DriveSuccess(existingConnection))
        }
    }

    fun removeDriveConnect(activity: ExportActivity) {
        val signInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(Drive.SCOPE_FILE)
                .build()
        val client = GoogleSignIn.getClient(activity, signInOptions)
        client.revokeAccess()
    }

    fun requestContentConnection(activity: ExportActivity, onConnected: (Connection) -> Unit) {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT_TREE)
        activity.registerActivityResult(REQUEST_EXTERNAL_ACCESS) { resultCode, resultIntent ->
            val uri = resultIntent?.data
            if (resultCode == RESULT_OK && uri != null) {
                activity.contentResolver.takePersistableUriPermission(
                        uri,
                        Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION
                )
                onConnected.invoke(Connection.ContentSuccess(uri))
            } else {
                onConnected.invoke(Connection.Failure)
            }
        }
        activity.startActivityForResult(intent, REQUEST_EXTERNAL_ACCESS)
    }

    fun checkContentConnection(contentResolver: ContentResolver, contentUri: Uri, onConnected: (Connection) -> Unit) {
        if (contentResolver.persistedUriPermissions.any{ it.uri == contentUri && it.isWritePermission}) {
            onConnected.invoke(Connection.ContentSuccess(contentUri))
        }
    }

    fun removeContentConnection(contentResolver: ContentResolver, contentUri: Uri) {
        contentResolver.releasePersistableUriPermission(contentUri, Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
    }
}
