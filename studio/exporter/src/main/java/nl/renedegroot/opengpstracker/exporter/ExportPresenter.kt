/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.opengpstracker.exporter

import android.content.ContentResolver
import android.net.Uri
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.google.android.gms.drive.DriveResourceClient
import nl.renedegroot.android.gpstracker.utils.preference.StringPreference
import nl.renedegroot.android.gpstracker.utils.preference.preference
import nl.renedegroot.opengpstracker.exporter.Target.ContentDrive
import nl.renedegroot.opengpstracker.exporter.Target.GoogleDrive
import nl.renedegroot.opengpstracker.exporter.target.Callback
import nl.renedegroot.opengpstracker.exporter.target.Connection
import nl.renedegroot.opengpstracker.exporter.target.ExportProcess
import nl.renedegroot.opengpstracker.exporter.target.content.DocumentUploadTask
import nl.renedegroot.opengpstracker.exporter.target.drive.DriveUploadTask

internal class ExportPresenter(
        private val viewModel: ExportViewModel,
        private val navigation: Navigation,
        private val exportProcess: ExportProcess,
        contentPreference: StringPreference
) : ViewModel() {

    private var contentUri by preference(contentPreference)
    private var driveResourceClient: DriveResourceClient? = null
    private val exportObserver = Observer<ExportProcess.ExportState> {
        onExportUpdate(it)
    }

    init {
        exportProcess.state.observeForever(exportObserver)
    }

    fun onTargetSelected(target: Target) {
        viewModel.selectedTarget.set(target)
        viewModel.status.set(Status.Prepared)
        when (viewModel.selectedTarget.get()) {
            is GoogleDrive -> navigation.checkDriveConnection()
            is ContentDrive -> contentUri.asUri()?.let { navigation.checkContentConnection(it) }
        }
    }

    fun onConnectClicked() {
        if (viewModel.status.get() == Status.Prepared) {
            viewModel.status.set(Status.Connected)
            when (viewModel.selectedTarget.get()) {
                is GoogleDrive -> navigation.connectToDrive()
                is ContentDrive -> navigation.connectToContent()
            }
        } else  {
            viewModel.showPrepare()
            when (viewModel.selectedTarget.get()) {
                is GoogleDrive -> navigation.disconnectDrive()
                is ContentDrive -> contentUri.asUri()?.let { navigation.disconnectUri(it) }
            }
        }

    }

    fun onNextStepClicked(status: Status) =
            when (status) {
                Status.Prepared -> onConnectClicked()
                Status.Running -> navigation.cancel()
                Status.Finished -> navigation.done()
                Status.Error -> navigation.done()
                is Status.Connected -> export()
            }

    fun didConnect(connection: Connection) =
            when (connection) {
                is Connection.ContentSuccess -> onContentConnected(connection.uri)
                Connection.Failure -> onDisconnected()
                is Connection.DriveSuccess -> onDriveConnected(connection.resourceClient)
                Connection.DriveFailure -> onDisconnected()
            }

    fun onCancel() {
        exportProcess.stopExport()
    }

    override fun onCleared() {
        exportProcess.stopExport()
        exportProcess.state.removeObserver(exportObserver)
        super.onCleared()
    }

    private fun onExportUpdate(state: ExportProcess.ExportState?) = when (state) {
        null, ExportProcess.ExportState.Idle -> {
        }
        is ExportProcess.ExportState.Active -> {
            viewModel.showActive(state)
        }
        is ExportProcess.ExportState.Finished -> {
            viewModel.showFinished(state)
        }
        is ExportProcess.ExportState.Error -> {
            viewModel.status.set(Status.Error)
            navigation.showFailureMessage(state.message)
        }
    }

    private fun onContentConnected(uri: Uri) {
        viewModel.status.set(Status.Connected)
        this.contentUri = uri.toString()
    }

    private fun onDriveConnected(resource: DriveResourceClient) {
        viewModel.status.set(Status.Connected)
        this.driveResourceClient = resource
    }

    private fun onDisconnected() {
        viewModel.showPrepare()
        this.driveResourceClient = null
        this.contentUri = null
        navigation.showConnectFailureMessage()
    }

    private fun export() =
            when (checkNotNull(viewModel.selectedTarget.get())) {
                is ContentDrive -> exportProcess.startExport { contentResolver: ContentResolver, trackUri: Uri, callback: Callback ->
                    DocumentUploadTask(contentResolver, trackUri, callback, checkNotNull(contentUri.asUri()))
                }
                is GoogleDrive -> exportProcess.startExport { contentResolver: ContentResolver, trackUri: Uri, callback: Callback ->
                    DriveUploadTask(contentResolver, trackUri, callback, checkNotNull(driveResourceClient))
                }
            }
}

private fun String?.asUri(): Uri? =
        if (this == null) {
            null
        } else {
            Uri.parse(this)
        }
