/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.v2.wear

import android.app.Activity
import android.app.Fragment
import android.os.Bundle
import androidx.annotation.CallSuper

/**
 * A stripped replacement for HolderFragment found in ArchComponents
 */
class HolderFragment : Fragment() {

    var holding: Holdable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onDestroy() {
        holding?.onCleared()
        super.onDestroy()
    }

    companion object {

        const val TAG = "HolderFragment"

        fun <T : Holdable> of(activity: Activity, constructor: () -> T): T {
            val holdable: T
            var holderFragment = activity.fragmentManager.findFragmentByTag(HolderFragment.TAG)
            if (holderFragment == null) {
                holderFragment = HolderFragment()
                holdable = constructor()
                holderFragment.holding = holdable
                activity.fragmentManager.beginTransaction()
                        .add(holderFragment, HolderFragment.TAG)
                        .commit()
            } else {
                holdable = (holderFragment as HolderFragment).holding as T
            }

            return holdable
        }
    }

    interface Holdable {
        @CallSuper
        fun onCleared() {
        }
    }
}
