/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.features.trackedit

import nl.renedegroot.android.opengpstrack.ng.features.R
import org.hamcrest.Matchers.`is`
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class TrackTypeDescriptionsTest {

    lateinit var sut: TrackTypeDescriptions.Companion

    @Before
    fun setUp() {
        sut = TrackTypeDescriptions
    }

    @Test
    fun testSearchingBoatType() {
        // Act
        val trackType = sut.trackTypeForContentType("TYPE_BOAT")
        // Assert
        assertThat(trackType.drawableId, `is`(R.drawable.ic_track_type_boat))
        assertThat(trackType.stringId, `is`(R.string.track_type_boat))
        assertThat(trackType.contentValue, `is`("TYPE_BOAT"))
    }

    @Test
    fun testSearchingNullType() {
        // Act
        val trackType = sut.trackTypeForContentType(null)
        // Assert
        assertThat(trackType.drawableId, `is`(R.drawable.ic_track_type_default))
        assertThat(trackType.stringId, `is`(R.string.track_type_default))
        assertThat(trackType.contentValue, `is`("TYPE_DEFAULT"))
    }
}
