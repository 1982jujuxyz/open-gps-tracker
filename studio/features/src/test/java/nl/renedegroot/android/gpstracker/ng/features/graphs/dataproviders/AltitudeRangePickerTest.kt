package nl.renedegroot.android.gpstracker.ng.features.graphs.dataproviders

import android.content.Context
import android.content.res.Resources
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import nl.renedegroot.android.opengpstrack.ng.features.R
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.Is.`is`
import org.junit.Test

const val A_200_FEET_IN_METER = 60.96F
const val A_213_FEET_IN_METER = 64.9224F
const val A_198_FEET_IN_METER = 60.3504F

class AltitudeRangePickerTest {


    private val imperialResources: Resources = mock {
        on { getString(R.string.m_to_small_distance) } doReturn "0.30479999953"
    }
    private val imperialContext: Context = mock {
        on { resources } doReturn imperialResources
    }

    private val metricResources: Resources = mock {
        on { getString(R.string.m_to_small_distance) } doReturn "1"
    }
    private val metricContext: Context = mock {
        on { resources } doReturn metricResources
    }

    val sut = AltitudeRangePicker()

    @Test
    fun `couple of meter to zero`() {
        val pretty = sut.prettyMinYValue(metricContext, 3F)

        assertThat(pretty, `is`(0F))
    }

    @Test
    fun `near 100 of meter to zero`() {
        val pretty = sut.prettyMinYValue(metricContext, 99F)

        assertThat(pretty, `is`(0F))
    }

    @Test
    fun `over 200 of meter to 200`() {
        val pretty = sut.prettyMinYValue(metricContext, 229F)

        assertThat(pretty, `is`(200F))
    }

    @Test
    fun `couple of feet to zero`() {
        val pretty = sut.prettyMinYValue(imperialContext, 3F)

        assertThat(pretty, `is`(0F))
    }

    @Test
    fun `near 200 of feet to zero`() {
        val pretty = sut.prettyMinYValue(imperialContext, A_198_FEET_IN_METER)

        assertThat(pretty, `is`(0F))
    }

    @Test
    fun `over 200 of feet to 200`() {
        val pretty = sut.prettyMinYValue(imperialContext, A_213_FEET_IN_METER)

        assertThat(pretty, `is`(A_200_FEET_IN_METER))
    }
}
