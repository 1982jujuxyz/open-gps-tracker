/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.features.graphs.dataproviders

import android.content.Context
import android.content.res.Resources
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import nl.renedegroot.android.gpstracker.ng.features.graphs.GraphSpeedConverter
import nl.renedegroot.android.gpstracker.ng.features.util.MockAppComponentTestRule
import nl.renedegroot.android.opengpstrack.ng.features.R
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Rule
import org.junit.Test

class SpeedRangePickerTest {

    private val resources: Resources = mock {
        on { getString(R.string.mps_to_speed) } doReturn "3.6"
    }
    private val context: Context = mock {
        on { resources } doReturn resources
    }
    @get:Rule
    var appComponentRule = MockAppComponentTestRule()

    @Test
    fun `lower bound convert 9 kph to 0 kph`() {
        val sut = SpeedRangePicker(false)
        val speed = 9F / 3.6F

        val pretty = sut.prettyMinYValue(context, speed)

        assertThat(pretty, `is`(0F))
    }

    @Test
    fun `lower bound convert 28 kph to 20 kph`() {
        val sut = SpeedRangePicker(false)
        val speed = 28F / 3.6F

        val pretty = sut.prettyMinYValue(context, speed)

        assertThat(pretty, `is`(5.555556F))
    }

    @Test
    fun `upper bound convert 9 kph to 20 kph`() {
        val sut = SpeedRangePicker(false)
        val speed = 9F / 3.6F

        val pretty = sut.prettyMaxYValue(context, speed)

        assertThat(pretty, `is`(5.555556F))
    }

    @Test
    fun `upper bound convert 18 kph to 20 kph`() {
        val sut = SpeedRangePicker(false)
        val speed = 18F / 3.6F

        val pretty = sut.prettyMaxYValue(context, speed)

        assertThat(pretty, `is`(5.555556F))
    }

    @Test
    fun `lower bound convert 3_15 min-km to 3_00 min-km`() {
        val sut = SpeedRangePicker(true)
        sut.graphSpeedConverter = GraphSpeedConverter()
        val speed = 3.25F

        val pretty = sut.prettyMinYValue(context, speed)
        assertThat(pretty, `is`(3F))
    }

    @Test
    fun `upper bound convert 3_15 min-km to 4_00 min-km`() {
        val sut = SpeedRangePicker(true)
        sut.graphSpeedConverter = GraphSpeedConverter()
        val speed = 3.25F

        val pretty = sut.prettyMaxYValue(context, speed)
        assertThat(pretty, `is`(4F))
    }

}
