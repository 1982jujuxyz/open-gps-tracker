/*
 * Open GPS Tracker
 * Copyright (C) 2019 René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses>.
 */

package nl.renedegroot.android.gpstracker.ng.features.activityrecognition

import com.google.android.gms.location.ActivityTransition.ACTIVITY_TRANSITION_ENTER
import com.google.android.gms.location.ActivityTransition.ACTIVITY_TRANSITION_EXIT
import com.google.android.gms.location.ActivityTransitionEvent
import com.google.android.gms.location.DetectedActivity.RUNNING
import com.google.android.gms.location.DetectedActivity.UNKNOWN
import com.google.android.gms.location.DetectedActivity.WALKING
import nl.renedegroot.android.gpstracker.ng.features.trackedit.TrackTypeDescriptions
import nl.renedegroot.android.gpstracker.ng.features.trackedit.TrackTypeDescriptions.Companion.trackTypeForContentType
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.nullValue
import org.junit.Assert.assertThat
import org.junit.Test

private const val SECOND = 1_000_000_000L
private const val ONE_MINUTE = 60 * SECOND
private const val TWO_MINUTE = 2 * ONE_MINUTE
private const val THREE_MINUTE = 3 * ONE_MINUTE
private const val FOUR_MINUTE = 4 * ONE_MINUTE
private const val FIVE_MINUTE = 5 * ONE_MINUTE

class ActivityRecognitionIntentServiceTest {

    private val sut = ActivityRecognitionIntentService()

    @Test
    fun `missing activity`() {
        val activities = emptyList<ActivityTransitionEvent>()

        val result = sut.trackTypeForTransitions(activities, SECOND)

        assertThat(result, nullValue())
    }

    @Test
    fun `unknown activity`() {
        val activities = listOf(
                ActivityTransitionEvent(UNKNOWN, ACTIVITY_TRANSITION_ENTER, TWO_MINUTE)
        )

        val result = sut.trackTypeForTransitions(activities, FIVE_MINUTE)

        assertThat(result, `is`(trackTypeForContentType(TrackTypeDescriptions.VALUE_TYPE_DEFAULT)))
    }

    @Test
    fun `start running`() {
        val activities = listOf(
                ActivityTransitionEvent(RUNNING, ACTIVITY_TRANSITION_ENTER, SECOND)
        )

        val result = sut.trackTypeForTransitions(activities, FIVE_MINUTE)

        assertThat(result, `is`(trackTypeForContentType(TrackTypeDescriptions.VALUE_TYPE_RUN)))
    }

    @Test
    fun `finish run with walking`() {
        val activities = listOf(
                ActivityTransitionEvent(RUNNING, ACTIVITY_TRANSITION_ENTER, FOUR_MINUTE),
                ActivityTransitionEvent(RUNNING, ACTIVITY_TRANSITION_EXIT, ONE_MINUTE),
                ActivityTransitionEvent(WALKING, ACTIVITY_TRANSITION_ENTER, SECOND)
        )

        val result = sut.trackTypeForTransitions(activities, FIVE_MINUTE)

        assertThat(result, `is`(trackTypeForContentType(TrackTypeDescriptions.VALUE_TYPE_RUN)))
    }

    @Test
    fun `quick run with long walk`() {
        val activities = listOf(
                ActivityTransitionEvent(RUNNING, ACTIVITY_TRANSITION_ENTER, FOUR_MINUTE),
                ActivityTransitionEvent(RUNNING, ACTIVITY_TRANSITION_EXIT, THREE_MINUTE),
                ActivityTransitionEvent(WALKING, ACTIVITY_TRANSITION_ENTER, THREE_MINUTE)
        )

        val result = sut.trackTypeForTransitions(activities, FIVE_MINUTE)

        assertThat(result, `is`(trackTypeForContentType(TrackTypeDescriptions.VALUE_TYPE_WALK)))
    }
}
