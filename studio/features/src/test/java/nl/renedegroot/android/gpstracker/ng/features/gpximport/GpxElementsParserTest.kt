/*
 * Open GPS Tracker
 * Copyright (C) 2019 René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses>.
 */

package nl.renedegroot.android.gpstracker.ng.features.gpximport

import android.content.ContentResolver
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doAnswer
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import nl.renedegroot.android.gpstracker.service.db.DatabaseConstants
import nl.renedegroot.android.gpstracker.service.db.DatabaseConstants.Segments.SEGMENTS
import nl.renedegroot.android.gpstracker.service.util.segmentUri
import nl.renedegroot.android.gpstracker.service.util.trackUri
import nl.renedegroot.android.gpstracker.service.util.tracksUri
import nl.renedegroot.android.gpstracker.utils.contentprovider.append
import org.intellij.lang.annotations.Language
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner


@Language("XML")
private const val tracks = """
    <gpx>
       <trk><name>One</name></trk>
        <trk><name>Two</name></trk>
        <trk><name>Three</name></trk>
    </gpx>"""

@Language("XML")
private const val time = """
    <gpx>
        <trk>
            <trkseg>
                <trkpt lat="0.0" lon="0.0">
                    <time>%s</time>
                </trkpt>
            </trkseg>
        </trk>
    </gpx>"""

@RunWith(RobolectricTestRunner::class)
class GpxElementsParserTest {

    private val contentResolver: ContentResolver = mock {
        var trackId = 1L
        var segmentId = 1L
        whenever(it.insert(tracksUri(), null)).doAnswer { trackUri(trackId++) }
        whenever(it.insert(trackUri(1).append(SEGMENTS), null)).doAnswer { segmentUri(1, segmentId++) }
    }

    @Test
    fun parseTracks() {
        val parser = GpxElementsParser(contentResolver, "")

        parser.parse(tracks.byteInputStream())

        verify(contentResolver, times(3)).insert(tracksUri(), null)
        verify(contentResolver).update(eq(trackUri(1)), any(), eq(null), eq(null))
        verify(contentResolver).update(eq(trackUri(2)), any(), eq(null), eq(null))
        verify(contentResolver).update(eq(trackUri(3)), any(), eq(null), eq(null))
    }


    @Test
    fun parseExampleTime() {
        val parser = GpxElementsParser(contentResolver, "")

        parser.parse(time.format("2002-02-27T17:18:33Z").byteInputStream())
    }
}
