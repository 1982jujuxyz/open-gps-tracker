/*
 * Open GPS Tracker
 * Copyright (C) 2019 René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses>.
 */

package nl.renedegroot.android.gpstracker.ng.features.gpximport

import android.content.ContentResolver
import android.content.ContentValues
import android.net.Uri
import android.util.Xml
import androidx.core.content.contentValuesOf
import com.crashlytics.android.Crashlytics
import nl.renedegroot.android.gpstracker.service.db.DatabaseConstants.Segments.SEGMENTS
import nl.renedegroot.android.gpstracker.service.db.DatabaseConstants.Waypoints.ACCURACY
import nl.renedegroot.android.gpstracker.service.db.DatabaseConstants.Waypoints.SPEED
import nl.renedegroot.android.gpstracker.service.db.DatabaseConstants.Waypoints.WAYPOINTS
import nl.renedegroot.android.gpstracker.service.integration.ContentConstants.TracksColumns.CREATION_TIME
import nl.renedegroot.android.gpstracker.service.integration.ContentConstants.TracksColumns.NAME
import nl.renedegroot.android.gpstracker.service.integration.ContentConstants.Waypoints.ALTITUDE
import nl.renedegroot.android.gpstracker.service.integration.ContentConstants.Waypoints.LATITUDE
import nl.renedegroot.android.gpstracker.service.integration.ContentConstants.Waypoints.LONGITUDE
import nl.renedegroot.android.gpstracker.service.integration.ContentConstants.Waypoints.TIME
import nl.renedegroot.android.gpstracker.service.integration.ContentConstants.WaypointsColumns.BEARING
import nl.renedegroot.android.gpstracker.service.util.tracksUri
import nl.renedegroot.android.gpstracker.utils.contentprovider.append
import nl.renedegroot.android.gpstracker.utils.stream.UnicodeReader
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserException
import java.io.BufferedInputStream
import java.io.IOException
import java.io.InputStream
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.min

internal class GpxElementsParser(
        private val contentResolver: ContentResolver,
        private val defaultName: String
) {
    private val parser = Xml.newPullParser()

    @Throws(XmlPullParserException::class, IOException::class)
    fun parse(inputStream: InputStream): List<Uri> {
        val bis = BufferedInputStream(inputStream, 1024 * 8192)
        val reader = UnicodeReader(bis, "UTF-8")
        parser.setInput(reader)
        parser.nextTag()
        return readTracks()
    }

    private fun readTracks(): List<Uri> {
        val tracks = mutableListOf<Uri>()
        parser.require(XmlPullParser.START_TAG, null, "gpx")
        while (parser.next() != XmlPullParser.END_DOCUMENT) {
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }
            when (parser.name) {
                "trk" -> tracks.add(readTrack())
                else -> skip()
            }
        }
        return tracks
    }

    private fun readTrack(): Uri {
        parser.require(XmlPullParser.START_TAG, null, "trk")
        val trackUri = checkNotNull(contentResolver.insert(tracksUri(), null)) { "Must be able to insert track" }
        var name: String? = null
        var creationTime: Long? = null
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }
            when (parser.name) {
                "name" -> name = readName()
                "trkseg" -> creationTime = earliest(creationTime, readSegment(trackUri))
                else -> skip()
            }
        }
        creationTime?.let {
            contentResolver.update(trackUri, contentValuesOf(CREATION_TIME to it), null, null)
        }
        contentResolver.update(trackUri, contentValuesOf(NAME to (name ?: defaultName)), null, null)

        return trackUri
    }

    private fun earliest(time: Long?, other: Long?) =
            when {
                time == null || time == 0L -> other
                other == null || other == 0L -> time
                else -> min(time, other)
            }

    private fun readSegment(trackUri: Uri): Long? {
        parser.require(XmlPullParser.START_TAG, null, "trkseg")
        val segmentUri = checkNotNull(contentResolver.insert(trackUri.append(SEGMENTS), null)) { "Must be able to insert track segment" }
        val waypoints = mutableListOf<ContentValues>()
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }
            when (parser.name) {
                "trkpt" -> waypoints.add(readTrackPoint())
                else -> skip()
            }
        }
        if (waypoints.any { it.getAsLong(TIME) == 0L }) {
            if (!waypoints.all { it.getAsLong(TIME) == 0L }) {
                waypoints.forEach { it.put(TIME, 0L) }
            }
        } else {
            waypoints.sortBy { it.getAsLong(TIME) }
        }
        contentResolver.bulkInsert(segmentUri.append(WAYPOINTS), waypoints.toTypedArray())

        return waypoints.firstOrNull()?.getAsLong(TIME)
    }

    private fun readTrackPoint(): ContentValues {
        parser.require(XmlPullParser.START_TAG, null, "trkpt")
        val waypointValues = ContentValues()
        (0..1).forEach {
            when (parser.getAttributeName(it)) {
                "lat" -> waypointValues.put(LATITUDE, parser.getAttributeValue(it).toDouble())
                "lon" -> waypointValues.put(LONGITUDE, parser.getAttributeValue(it).toDouble())
            }
        }
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }
            when (parser.name) {
                "time" -> readText().toTime()?.let { waypointValues.put(TIME, it) }
                "ele" -> waypointValues.put(ALTITUDE, readText().toDouble())
                "extensions" -> readExtension(waypointValues)
                else -> skip()
            }
        }
        waypointValues.ensure(SPEED, 0.0)
        waypointValues.ensure(TIME, 0L)
        waypointValues.ensure(ALTITUDE, 0.0)
        return waypointValues
    }

    private fun readExtension(waypointValues: ContentValues) {
        parser.require(XmlPullParser.START_TAG, null, "extensions")
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }
            when (parser.name) {
                "speed" -> waypointValues.put(SPEED, readText().toDouble())
                "accuracy" -> waypointValues.put(ACCURACY, readText().toDouble())
                "course" -> waypointValues.put(BEARING, readText().toDouble())
                else -> skip()
            }
        }
    }

    private fun skip() {
        check(parser.eventType == XmlPullParser.START_TAG)
        var depth = 1
        while (depth != 0) {
            when (parser.next()) {
                XmlPullParser.END_TAG -> depth--
                XmlPullParser.START_TAG -> depth++
            }
        }
    }

    private fun readName(): String {
        parser.require(XmlPullParser.START_TAG, null, "name")
        val name = readText()
        parser.require(XmlPullParser.END_TAG, null, "name")
        return name
    }

    private fun readText(): String {
        var result = ""
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.text
            parser.nextTag()
        }
        return result
    }

    private val zuluDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US)
    private val zuluDateFormatMs = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US)
    private val zuluDateFormatUtc = SimpleDateFormat("yyyy-MM-dd HH:mm:ss 'UTC'", Locale.US)

    init {
        val utc = TimeZone.getTimeZone("UTC");
        zuluDateFormat.timeZone = utc // ZULU_DATE_FORMAT format ends with Z for UTC so make that true
        zuluDateFormatMs.timeZone = utc
    }

    private fun String.toTime(): Long? = try {
        when (length) {
            20 -> synchronized(zuluDateFormat) { zuluDateFormat.parse(this).time }
            23 -> synchronized(zuluDateFormatUtc) { zuluDateFormatUtc.parse(this).time }
            24 -> synchronized(zuluDateFormatMs) { zuluDateFormatMs.parse(this).time }
            else -> throw ParseException("Unable to parse dateTime $this of length $length", 0)
        }
    } catch (e: ParseException) {
        Crashlytics.logException(e)
        null
    }
}

private fun ContentValues.ensure(key: String, value: Double) {
    if (!this.containsKey(key)) {
        put(key, value)
    }
}

private fun ContentValues.ensure(key: String, value: Long) {
    if (!this.containsKey(key)) {
        put(key, value)
    }
}
