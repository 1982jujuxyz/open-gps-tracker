/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.features.util

import android.net.Uri
import nl.renedegroot.android.gpstracker.integration.IGPSLoggerStateCallback
import nl.renedegroot.android.gpstracker.service.integration.ServiceConstants.STATE_LOGGING
import nl.renedegroot.android.gpstracker.service.integration.ServiceConstants.STATE_PAUSED
import nl.renedegroot.android.gpstracker.service.integration.ServiceConstants.STATE_STOPPED
import nl.renedegroot.android.gpstracker.service.integration.ServiceManager
import nl.renedegroot.android.gpstracker.service.util.trackUri
import timber.log.Timber
import javax.inject.Inject

class LoggingStateController @Inject constructor(
        private val serviceManager: ServiceManager
) {
    private var listener: LoggingStateListener? = null
    val lastWaypoint: Uri?
        get() = serviceManager.getLastWaypoint()
    val trackUri: Uri?
        get() = trackUri(serviceManager.getTrackId())
    val loggingState: Int
        get() = serviceManager.getLoggingState()

    fun connect(connectListener: LoggingStateListener? = null) {
        Timber.d("Will connect for $connectListener")
        this.listener = connectListener
        serviceManager.startup {
            synchronized(this) {
                val loggingState = serviceManager.getLoggingState()
                val trackId = serviceManager.getTrackId()
                Timber.d("onConnect LoggerState %d", loggingState)
                when (loggingState) {
                    STATE_LOGGING -> listener?.didStartLogging(trackUri(trackId))
                    STATE_PAUSED -> listener?.didPauseLogging(trackUri(trackId))
                    STATE_STOPPED -> listener?.didStopLogging()
                }
                serviceManager.addObserver(callback)
            }
        }
    }

    fun disconnect() {
        Timber.d("Will disconnect for $listener")
        serviceManager.removeObserver(callback)
        serviceManager.shutdown()
        listener = null
    }

    private val callback = object : IGPSLoggerStateCallback.Stub() {
        override fun asBinder() = this

        override fun didPauseLogging(trackUri: Uri) {
            listener?.didPauseLogging(trackUri)
        }

        override fun didStartLogging(trackUri: Uri) {
            listener?.didStartLogging(trackUri)
        }

        override fun didStopLogging() {
            listener?.didStopLogging()
        }

    }
}

fun LoggingStateController.isLogging() = loggingState == STATE_LOGGING

interface LoggingStateListener {
    fun didStopLogging()
    fun didPauseLogging(trackUri: Uri)
    fun didStartLogging(trackUri: Uri)
}
