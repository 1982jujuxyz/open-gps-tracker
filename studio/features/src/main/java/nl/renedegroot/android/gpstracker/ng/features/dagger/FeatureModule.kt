/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.features.dagger

import android.content.Context
import com.google.android.gms.location.ActivityRecognitionClient
import dagger.Module
import dagger.Provides
import nl.renedegroot.android.gpstracker.ng.features.gpxexport.AUTHORITY
import nl.renedegroot.android.gpstracker.ng.features.gpximport.GpxImportController
import nl.renedegroot.android.gpstracker.ng.features.gpximport.GpxParserFactory
import nl.renedegroot.android.gpstracker.ng.features.map.TrackReaderFactory
import nl.renedegroot.android.gpstracker.ng.features.map.rendering.TrackRenderProviderFactory
import nl.renedegroot.android.gpstracker.ng.features.summary.SummaryCalculator
import nl.renedegroot.android.gpstracker.ng.features.trackedit.TrackTypeDescriptions
import nl.renedegroot.android.gpstracker.ng.features.tracklist.ImportNotification
import nl.renedegroot.android.gpstracker.ng.features.wear.StatisticsCollector
import nl.renedegroot.android.gpstracker.service.integration.ServiceManager
import nl.renedegroot.android.gpstracker.utils.VersionHelper
import nl.renedegroot.android.gpstracker.utils.concurrent.ExecutorFactory
import nl.renedegroot.android.gpstracker.v2.sharedwear.datasync.DataSender
import nl.renedegroot.android.gpstracker.v2.sharedwear.messaging.MessageSenderFactory
import nl.renedegroot.android.gpstracker.v2.sharedwear.util.LocaleProvider
import nl.renedegroot.android.gpstracker.v2.sharedwear.util.StatisticsFormatter
import nl.renedegroot.android.gpstracker.v2.sharedwear.util.TimeSpanCalculator
import nl.renedegroot.android.opengpstrack.ng.note.NoteMarkerController
import javax.inject.Named

@Module
class FeatureModule(context: Context) {

    private val context = context.applicationContext!!

    @FeatureScope
    @Provides
    fun importNotification() = ImportNotification(context)

    @Provides
    fun summaryCalculator() = SummaryCalculator()

    @Provides
    fun trackReaderFactory() = TrackReaderFactory()

    @Provides
    fun trackTileProviderFactory() = TrackRenderProviderFactory()

    @Provides
    fun trackTypeDescriptions() = TrackTypeDescriptions()

    @Provides
    @Named("shareProviderAuthority")
    fun shareProviderAuthority(): String {
        return AUTHORITY
    }

    @Provides
    fun gpxParser() = GpxParserFactory(context)

    @Provides
    fun gpxImportController() = GpxImportController(context)

    @Provides
    fun executorFactory() = ExecutorFactory()

    @FeatureScope
    @Provides
    fun statisticsCollector() = StatisticsCollector()

    @Provides
    fun statisticsFormatting(timeSpanUtil: TimeSpanCalculator) = StatisticsFormatter(LocaleProvider(), timeSpanUtil)

    @Provides
    fun messageSenderFactory() = MessageSenderFactory()

    @Provides
    fun dataSender() = DataSender(context)

    @Provides
    fun timeSpanCalculator() = TimeSpanCalculator()

    @Provides
    fun versionHelper() = VersionHelper()

    @Provides
    fun activityRecognitionClient() = ActivityRecognitionClient(context)

    @Provides
    fun provideNoteMarkerController() = NoteMarkerController(context)

    @Provides
    fun serviceManager() = ServiceManager(context)
}
