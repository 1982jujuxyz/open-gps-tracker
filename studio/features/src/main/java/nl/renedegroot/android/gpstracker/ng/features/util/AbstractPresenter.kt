/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.features.util

import androidx.annotation.CallSuper
import androidx.annotation.WorkerThread
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.launch
import nl.renedegroot.android.gpstracker.ng.base.BaseConfiguration
import nl.renedegroot.android.gpstracker.utils.viewmodel.CoroutineViewModel
import java.util.concurrent.Executors

/**
 * Basis for a long lived (@see android.arch.lifecycle.ViewModel) Presenter that
 * can start / stop based on visibility to the user.
 *
 * Instantiate using the android.arch.lifecycle.ViewModel factory means.
 */
abstract class AbstractPresenter: CoroutineViewModel() {
    private val changeDispatcher: CoroutineDispatcher = BaseConfiguration.appComponent.uiUpdateExecutor().asCoroutineDispatcher()
    private var firstStart = true
    private var started = false
    private var dirty = true

    /**
     * Called when the Presenter is visible to the user. Will call onChange() on initial
     * start and when markDirty() was called when not started.
     */
    fun start() {
        if (!started) {
            started = true
            if (firstStart) {
                firstStart = false
                onFirstStart()
            }
            onStart()
        }
        checkUpdate()
    }

    /**
     * Call when the Presenter is no longer started.
     */
    fun stop() {
        if (started) {
            started = false
            onStop()
        }
    }

    /**
     * Mark the Presenter as dirty when the update to the View would incur CPU, memory or other
     * load not useful when the presenter is started.
     */
    protected fun markDirty() {
        dirty = true
        checkUpdate()
    }

    /**
     * Called before right before the first time that @see onStart is called
     */
    @CallSuper
    open fun onFirstStart() {
    }

    /**
     * Override to start listening to model changes which incur load on the device. Such as
     * observing sensors or polling components.
     */
    @CallSuper
    open fun onStart() {
    }

    /**
     * Execute View updates deferred by using markDirty. Will run on a worker thread to allow
     * IO.
     */
    @WorkerThread
    abstract fun onChange()

    /**
     * Override to stop listening to model changes.
     */
    @CallSuper
    open fun onStop() {
    }

    private fun checkUpdate() {
        launch(changeDispatcher) {
            if (dirty && started) {
                dirty = false
                onChange()
            }
        }
    }
}
