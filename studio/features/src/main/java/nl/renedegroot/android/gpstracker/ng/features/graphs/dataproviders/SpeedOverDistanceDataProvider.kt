/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.features.graphs.dataproviders

import android.content.Context
import androidx.annotation.WorkerThread
import nl.renedegroot.android.gpstracker.ng.features.FeatureConfiguration
import nl.renedegroot.android.gpstracker.ng.features.graphs.GraphSpeedConverter
import nl.renedegroot.android.gpstracker.ng.features.graphs.widgets.GraphPoint
import nl.renedegroot.android.gpstracker.ng.features.summary.Summary
import nl.renedegroot.android.gpstracker.v2.sharedwear.util.StatisticsFormatter
import nl.renedegroot.android.opengpstrack.ng.features.R
import javax.inject.Inject

class SpeedOverDistanceDataProvider(private val inverseSpeed: Boolean) : GraphDataCalculator {

    @Inject
    lateinit var statisticsFormatter: StatisticsFormatter

    @Inject
    lateinit var graphSpeedConverter: GraphSpeedConverter

    private var _graphPoint: List<GraphPoint> = emptyList()
    override val graphPoint: List<GraphPoint>
        get() = _graphPoint

    private val speedRangePicker = SpeedRangePicker(inverseSpeed)

    override val yLabel: Int
        get() = R.string.graph_label_speed

    override val xLabel: Int
        get() = R.string.graph_label_distance

    init {
        FeatureConfiguration.featureComponent.inject(this)
    }

    override fun prettyMinYValue(context: Context, yValue: Float) =
            speedRangePicker.prettyMinYValue(context, yValue)

    override fun prettyMaxYValue(context: Context, yValue: Float) =
            speedRangePicker.prettyMaxYValue(context, yValue)

    @WorkerThread
    override fun calculateGraphPoints(summary: Summary) {
        val graphPoints = mutableListOf<GraphPoint>()
        summary.deltas.forEach {
            addSegmentToGraphPoints(it, graphPoints)
        }

        val meters = 75F
        _graphPoint = graphPoints
                .inverseSpeed()
                .flattenOutliers()
                .smooth(meters)
    }

    override fun describeYvalue(context: Context, yValue: Float): String {
        val speed = if (inverseSpeed) yValue.inverseSpeed() else yValue
        return statisticsFormatter.convertMeterPerSecondsToSpeed(context, speed, inverseSpeed, false)
    }

    override fun describeXvalue(context: Context, xValue: Float): String {
        return statisticsFormatter.convertMetersToDistance(context, xValue)
    }

    private fun addSegmentToGraphPoints(deltas: List<Summary.Delta>, graphPoints: MutableList<GraphPoint>) {
        deltas.forEach {
            val speed = it.deltaMeters / (it.deltaMilliseconds / 1000F)
            if (speed >= 0F && it.deltaMeters > 0F) {
                val distance = it.totalMeters - (it.deltaMeters / 2F)
                graphPoints.add(GraphPoint(distance, speed))
            }
        }
    }

    private fun List<GraphPoint>.inverseSpeed(): List<GraphPoint> {
        return if (inverseSpeed) {
            this.map { GraphPoint(it.x, it.y.inverseSpeed()) }
        } else {
            this
        }
    }

    private fun Float.inverseSpeed() = graphSpeedConverter.speedToYValue(this)
}

