/*
 * Open GPS Tracker
 * Copyright (C) 2019 René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses>.
 */

package nl.renedegroot.android.gpstracker.ng.features.map.rendering

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import androidx.databinding.Observable
import androidx.databinding.ObservableField
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import nl.renedegroot.android.gpstracker.ng.features.summary.Summary
import nl.renedegroot.android.opengpstrack.ng.features.R
import kotlin.coroutines.CoroutineContext

class StartEndMarkerProvider(val resources: Resources) : CoroutineScope {
    private val job = Job()
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Default + job

    private var googleMap: GoogleMap? = null
    private var summary: ObservableField<Summary>? = null
    private val modelCallback = object : Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(sender: Observable, propertyId: Int) {
            waypointsDidChange()
        }
    }
    private var endMarker: Marker? = null

    fun start(summary: ObservableField<Summary>, googleMap: GoogleMap) {
        this.summary = summary
        this.googleMap = googleMap
        summary.addOnPropertyChangedCallback(modelCallback)
        waypointsDidChange()
    }

    fun stop() {
        summary?.removeOnPropertyChangedCallback(modelCallback)
        endMarker?.remove()
        endMarker = null
        googleMap = null
    }

    private fun waypointsDidChange() {
        launch(Dispatchers.Main) {
            val points = summary?.get()?.waypoints
            val end = points?.lastOrNull()?.lastOrNull()
            endMarker?.remove()
            end?.let {
                endMarker = googleMap?.addMarker(MarkerOptions()
                        .position(LatLng(end.latitude, end.longitude))
                        .icon(BitmapDescriptorFactory.fromBitmap(resources.loadIntoBitmap(R.drawable.ic_person_pin_black_24dp)))
                )
            }
        }
    }
}

fun Resources.loadIntoBitmap(drawble: Int): Bitmap {
    val vectorDrawable = checkNotNull(VectorDrawableCompat.create(this, drawble, null))
    val bitmap = Bitmap.createBitmap(vectorDrawable.intrinsicWidth,
            vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(bitmap)
    vectorDrawable.setBounds(0, 0, canvas.width, canvas.height)
    vectorDrawable.draw(canvas)

    return bitmap
}

