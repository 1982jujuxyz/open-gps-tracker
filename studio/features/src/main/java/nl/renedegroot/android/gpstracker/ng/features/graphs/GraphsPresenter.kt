/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.graphs

import android.net.Uri
import androidx.annotation.WorkerThread
import nl.renedegroot.android.gpstracker.ng.base.common.ofMainThread
import nl.renedegroot.android.gpstracker.ng.base.common.postMainThread
import nl.renedegroot.android.gpstracker.ng.features.FeatureConfiguration
import nl.renedegroot.android.gpstracker.ng.features.graphs.dataproviders.AltitudeOverDistanceDataProvider
import nl.renedegroot.android.gpstracker.ng.features.graphs.dataproviders.AltitudeOverTimeDataProvider
import nl.renedegroot.android.gpstracker.ng.features.graphs.dataproviders.SpeedOverDistanceDataProvider
import nl.renedegroot.android.gpstracker.ng.features.graphs.dataproviders.SpeedOverTimeDataProvider
import nl.renedegroot.android.gpstracker.ng.features.summary.Summary
import nl.renedegroot.android.gpstracker.ng.features.summary.SummaryManager
import nl.renedegroot.android.gpstracker.ng.features.util.AbstractSelectedTrackPresenter
import javax.inject.Inject

class GraphsPresenter : AbstractSelectedTrackPresenter() {

    @Inject
    lateinit var summaryManager: SummaryManager
    internal val viewModel = GraphsViewModel()
    private var trackSummary: Summary? = null
    private var runningSelection = false

    init {
        FeatureConfiguration.featureComponent.inject(this)
    }

    override fun onFirstStart() {
        super.onFirstStart()
        summaryManager.start()
    }

    override fun onCleared() {
        super.onCleared()
        summaryManager.stop()
    }

    //region AbstractSelectedTrackPresenter

    override fun onTrackUpdate(trackUri: Uri?, name: String) {
        viewModel.trackUri.set(trackUri)
        if (trackUri != null) {
            summaryManager.collectSummaryInfo(trackUri) { summary ->
                trackSummary = summary
                updateNumbers(summary)
                updateGraph(summary)
            }
        } else {
            resetTrack()
        }
    }

    //endregion

    //region View callbacks

    fun didSelectDistance() {
        runSelection {
            if (viewModel.distanceSelected.get()) return@runSelection
            viewModel.distanceSelected.set(true)
            viewModel.durationSelected.set(false)
            trackSummary?.let { summary ->
                updateGraph(summary)
            }
        }
    }

    fun didSelectTime() {
        runSelection {
            if (viewModel.durationSelected.get()) return@runSelection
            viewModel.distanceSelected.set(false)
            viewModel.durationSelected.set(true)
            trackSummary?.let { summary ->
                updateGraph(summary)
            }
        }
    }

    private fun runSelection(lambda: () -> Unit) {
        if (runningSelection) return
        runningSelection = true
        ofMainThread {
            lambda()
            postMainThread {
                runningSelection = false
            }
        }
    }

    //endregion

    //region update

    private fun resetTrack() {
        viewModel.distance.set(0F)
        viewModel.timeSpan.set(0L)
        viewModel.speed.set(0F)
        viewModel.waypoints.set("-")
        viewModel.startTime.set(0L)
        viewModel.duration.set(0L)
        viewModel.paused.set(0L)
    }

    @WorkerThread
    private fun updateGraph(summary: Summary) {
        val inverseSpeed = summary.type.isRunning()
        val speedGraphData = if (viewModel.durationSelected.get()) {
            SpeedOverTimeDataProvider(inverseSpeed)
        } else {
            SpeedOverDistanceDataProvider(inverseSpeed)
        }
        speedGraphData.calculateGraphPoints(summary)
        viewModel.graphSpeedDataProvider.set(speedGraphData)
        val altitudeGraphData = if (viewModel.durationSelected.get()) {
            AltitudeOverTimeDataProvider()
        } else {
            AltitudeOverDistanceDataProvider()
        }
        altitudeGraphData.calculateGraphPoints(summary)
        viewModel.graphAltitudeDataProvider.set(altitudeGraphData)
    }

    private fun updateNumbers(summary: Summary) {
        viewModel.waypoints.set(summary.count.toString())
        viewModel.startTime.set(summary.startTimestamp)
        val total = summary.stopTimestamp - summary.startTimestamp
        val pausedTime = total - summary.trackedPeriod
        viewModel.paused.set(pausedTime)
        viewModel.distance.set(summary.distance)
        viewModel.duration.set(summary.trackedPeriod)
        viewModel.timeSpan.set(total)

        val seconds = summary.trackedPeriod / 1000F
        val speed = if (seconds > 0) summary.distance / seconds else 0F
        viewModel.speed.set(speed)

        viewModel.inverseSpeed.set(summary.type.isRunning())
    }
}

