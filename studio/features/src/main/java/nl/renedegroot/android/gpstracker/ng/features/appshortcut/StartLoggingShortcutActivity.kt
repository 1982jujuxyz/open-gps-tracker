/*
 * Open GPS Tracker
 * Copyright (C) 2019 René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses>.
 */

package nl.renedegroot.android.gpstracker.ng.features.appshortcut

import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import nl.renedegroot.android.gpstracker.service.integration.ServiceCommander
import nl.renedegroot.android.opengpstrack.ng.features.R

class StartLoggingShortcutActivity : FragmentActivity() {

    private val viewModel by lazy { ViewModelProviders.of(this).get(StartLoggingViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start_logging_service)
        ServiceCommander(this)
                .startGPSLogging()
        viewModel.shouldFinish.observe(this, Observer {
            if (it == true) {
                finish()
            }
        })
    }

    override fun onResume() {
        super.onResume()
        viewModel.didDisplay()

    }
}

internal class StartLoggingViewModel : ViewModel(), CoroutineScope by CoroutineScope(Dispatchers.Default) {

    val shouldFinish = MutableLiveData<Boolean>()

    fun didDisplay() = launch {
        delay(4000)
        shouldFinish.postValue(true)
    }
}
