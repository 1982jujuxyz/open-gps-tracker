/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.gpxexport

import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.core.content.FileProvider.getUriForFile
import nl.renedegroot.android.gpstracker.ng.base.BuildConfig
import nl.renedegroot.android.gpstracker.ng.base.dagger.DiskIO
import nl.renedegroot.android.gpstracker.ng.features.FeatureConfiguration
import nl.renedegroot.android.gpstracker.utils.file.cacheFile
import nl.renedegroot.opengpstracker.exporter.gpx.GpxCreator
import nl.renedegroot.opengpstracker.exporter.gpx.GpxCreator.MIME_TYPE_GPX
import java.io.FileOutputStream
import java.util.concurrent.Executor
import javax.inject.Inject

const val MIME_TYPE_GENERAL = "application/octet-stream"
const val MIME_TYPE_GPX = "application/gpx+xml"
const val MIME_TYPE_ANY = "*/*"
const val AUTHORITY = BuildConfig.providerAuthority + ".gpxshareprovider"

class ShareIntentFactory @Inject constructor(
        val context: Context,
        val resolver: ContentResolver,
        @DiskIO val executor: Executor) {

    init {
        FeatureConfiguration.featureComponent.inject(this)
    }

    fun createShareIntent(trackUri: Uri, completion: (Intent) -> Unit) {
        executor.execute {
            val fileName = GpxCreator.fileName(trackUri)
            val exportFile = context.cacheFile("shared-gpx", fileName)
            val outputStream = FileOutputStream(exportFile)
            GpxCreator(resolver, trackUri).createGpx(outputStream)

            val contentUri = getUriForFile(context, AUTHORITY, exportFile)
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.data = contentUri
            shareIntent.putExtra(Intent.EXTRA_STREAM, contentUri)
            shareIntent.type = MIME_TYPE_GPX

            completion(shareIntent)
        }
    }
}
