/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.features.tracklist

import android.net.Uri
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.databinding.BindingAdapter
import nl.renedegroot.android.gpstracker.ng.base.common.postMainThread
import nl.renedegroot.android.opengpstrack.ng.features.R
import timber.log.Timber

@BindingAdapter("tracks")
fun setTracks(recyclerView: androidx.recyclerview.widget.RecyclerView, tracks: List<Uri>?) {
    val viewAdapter: TrackListViewAdapter
    if (recyclerView.adapter is TrackListViewAdapter) {
        viewAdapter = recyclerView.adapter as TrackListViewAdapter
        viewAdapter.updateTracks(tracks ?: emptyList())
    } else {
        viewAdapter = TrackListViewAdapter(recyclerView.context)
        viewAdapter.updateTracks(tracks ?: emptyList())
        recyclerView.adapter = viewAdapter
    }
}

@BindingAdapter("selected")
fun setTracks(recyclerView: androidx.recyclerview.widget.RecyclerView, track: Uri?) {
    val viewAdapter: TrackListViewAdapter
    if (recyclerView.adapter is TrackListViewAdapter) {
        viewAdapter = recyclerView.adapter as TrackListViewAdapter
        viewAdapter.selection = track
    } else {
        viewAdapter = TrackListViewAdapter(recyclerView.context)
        viewAdapter.selection = track
        recyclerView.adapter = viewAdapter
    }
}

@BindingAdapter("tracksListener")
fun setListener(recyclerView: androidx.recyclerview.widget.RecyclerView, listener: TrackListAdapterListener?) {
    val adapter = recyclerView.adapter
    if (adapter != null && adapter is TrackListViewAdapter) {
        adapter.listener = listener
    } else {
        Timber.e("Binding listener when missing adapter, are the xml attributes out of order")
    }
}

@BindingAdapter("editMode")
fun setEditMode(card: androidx.cardview.widget.CardView, editMode: Boolean) {
    val share = card.findViewById<View>(R.id.row_track_share)
    val delete = card.findViewById<View>(R.id.row_track_delete)
    val edit = card.findViewById<View>(R.id.row_track_edit)
    if (editMode) {
        if (share.visibility != VISIBLE) {
            share.alpha = 0F
            edit.alpha = 0F
            delete.alpha = 0F
        }
        share.visibility = VISIBLE
        edit.visibility = VISIBLE
        delete.visibility = VISIBLE
        share.animate().alpha(1.0F)
        edit.animate().alpha(1.0F)
        delete.animate().alpha(1.0F)
    } else if (share.visibility == VISIBLE) {
        share.animate().alpha(0.0F)
        edit.animate().alpha(0.0F)
        delete.animate().alpha(0.0F).withEndAction {
            share.visibility = GONE
            edit.visibility = GONE
            delete.visibility = GONE
        }
    }
}

@BindingAdapter("focusPosition")
fun setFocusPosition(list: androidx.recyclerview.widget.RecyclerView, position: Int?) {
    if (position != null && position > 0) {
        postMainThread { list.layoutManager?.scrollToPosition(position) }
    }
}
