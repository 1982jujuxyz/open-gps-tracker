/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.tracklist

import android.content.Intent
import android.content.Intent.ACTION_OPEN_DOCUMENT
import android.content.Intent.ACTION_OPEN_DOCUMENT_TREE
import android.net.Uri
import android.os.Build
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import nl.renedegroot.android.gpstracker.ng.features.FeatureConfiguration
import nl.renedegroot.android.gpstracker.ng.features.gpxexport.MIME_TYPE_ANY
import nl.renedegroot.android.gpstracker.ng.features.gpximport.ImportTrackTypeDialogFragment
import nl.renedegroot.android.gpstracker.ng.features.track.TrackActivity
import nl.renedegroot.android.gpstracker.ng.features.trackdelete.TrackDeleteDialogFragment
import nl.renedegroot.android.gpstracker.ng.features.trackedit.TrackEditDialogFragment
import nl.renedegroot.android.gpstracker.ng.features.trackedit.TrackTypeDescriptions.Companion.KEY_META_FIELD_TRACK_TYPE
import nl.renedegroot.android.gpstracker.utils.VersionHelper
import nl.renedegroot.android.gpstracker.utils.activityresult.ActivityResultLambda
import nl.renedegroot.opengpstracker.exporter.ExportActivity
import javax.inject.Inject


private const val TAG_DIALOG = "DIALOG"

class TrackListNavigation(val fragment: androidx.fragment.app.Fragment) {

    @Inject
    lateinit var versionHelper: VersionHelper

    init {
        FeatureConfiguration.featureComponent.inject(this)
    }

    fun showTrackDeleteDialog(track: Uri) {
        TrackDeleteDialogFragment.newInstance(track)
                .show(fragment.requireFragmentManager(), TAG_DIALOG)
    }

    fun showTrackEditDialog(trackUri: Uri) {
        TrackEditDialogFragment.newInstance(trackUri)
                .show(fragment.requireFragmentManager(), TAG_DIALOG)
    }

    fun showIntentChooser(intent: Intent, @StringRes text: Int) {
        fragment.startActivity(Intent.createChooser(intent, fragment.getString(text)))
    }

    fun finishTrackSelection() =
        if (fragment.activity is TrackActivity) {
            // For tablet we'll opt to leave the track list on the screen instead of removing it
            // getSupportFragmentManager().popBackStack(TRANSACTION_TRACKS, POP_BACK_STACK_INCLUSIVE);
        } else {
            fragment.activity?.finish()
        }


    fun startGpxFileSelection(param: (Intent?) -> Unit) {
        if (versionHelper.isAtLeast(Build.VERSION_CODES.KITKAT)) {
            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.addCategory(Intent.CATEGORY_OPENABLE) // Intent.ACTION_OPEN_DOCUMENT has too few results
            intent.type = MIME_TYPE_ANY
            startTypeImport(intent, param)
        } else {
            val context = fragment.context
                    ?: throw IllegalStateException("Attempting to run select file outside lifecycle of fragment")
            AlertDialog.Builder(context)
                    .setTitle("Not implemented ")
                    .setMessage("This feature does not exist pre-KitKat")
                    .create()
                    .show()
        }
    }

    fun startGpxDirectorySelection(param: (Intent?) -> Unit) {
        if (versionHelper.isAtLeast(Build.VERSION_CODES.LOLLIPOP)) {
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT_TREE)
            startTypeImport(intent, param)
        } else {
            val context = fragment.context
                    ?: throw IllegalStateException("Attempting to run select directory outside lifecycle of fragment")
            AlertDialog.Builder(context)
                    .setTitle("Not implemented ")
                    .setMessage("This feature does not exist pre-Lollipop")
                    .create()
                    .show()
        }
    }

    private fun startTypeImport(intent: Intent, param: (Intent?) -> Unit) {
        if (fragment is ActivityResultLambda) {
            fragment.startActivityForResult(intent) { resultIntent ->
                resultIntent?.let {
                    val fragmentManager = fragment.fragmentManager
                            ?: throw IllegalStateException("Attempting to run import outside lifecycle of fragment")
                    ImportTrackTypeDialogFragment().show(fragmentManager, TAG_DIALOG) { type ->
                        resultIntent.putExtra(KEY_META_FIELD_TRACK_TYPE, type)
                        val takeFlags = resultIntent.flags and Intent.FLAG_GRANT_READ_URI_PERMISSION
                        val uri = resultIntent.data
                        val resolver = fragment.context?.contentResolver
                        if (uri != null && resolver != null && intent.action.fromStorageFramework()) {
                            resolver.takePersistableUriPermission(uri, takeFlags)
                        }
                        param(resultIntent)
                    }
                }
            }
        }
    }

    fun startGpxExport() {
        val context = fragment.context ?: return
        val intent = Intent(context, ExportActivity::class.java)
        context.startActivity(intent)
    }

}

private fun String?.fromStorageFramework() =
        this == ACTION_OPEN_DOCUMENT || this == ACTION_OPEN_DOCUMENT_TREE
