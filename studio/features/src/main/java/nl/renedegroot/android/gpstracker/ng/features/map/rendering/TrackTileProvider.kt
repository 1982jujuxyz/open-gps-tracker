/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.map.rendering

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Canvas
import androidx.databinding.Observable
import androidx.databinding.ObservableField
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Tile
import com.google.android.gms.maps.model.TileOverlay
import com.google.android.gms.maps.model.TileOverlayOptions
import com.google.android.gms.maps.model.TileProvider
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.coroutines.yield
import nl.renedegroot.android.gpstracker.ng.features.graphs.dataproviders.standardRange
import nl.renedegroot.android.gpstracker.ng.features.map.WeightType
import nl.renedegroot.android.gpstracker.ng.features.summary.Summary
import nl.renedegroot.android.gpstracker.ng.features.summary.speed
import nl.renedegroot.android.gpstracker.service.util.Waypoint
import nl.renedegroot.android.gpstracker.utils.throttle.ThrottleCounter
import nl.renedegroot.android.gpstracker.utils.throttle.throttle
import timber.log.Timber
import java.io.ByteArrayOutputStream
import kotlin.coroutines.CoroutineContext

private const val STROKE_WIDTH_DP = 4
private const val TILE_SIZE_DP = 256

class TrackTileProvider(resources: Resources) : TileProvider, CoroutineScope {
    private val job = Job()
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Default + job
    private val tileSize: Float
    private val strokeWidth: Float
    private var pathRenderer: PathRenderer? = null
    private lateinit var titleOverLay: TileOverlay
    private lateinit var summaryField: ObservableField<Summary>
    private lateinit var weightTypeField: ObservableField<WeightType>
    private val modelCallback = object : Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(sender: Observable, propertyId: Int) {
            val summary = summaryField.get()
            val weightType = weightTypeField.get() ?: WeightType.None
            if (summary != null) {
                waypointsDidChange(summary, weightType)
            }
        }
    }
    private val throttleCounter = ThrottleCounter(5000)

    init {
        val density = resources.displayMetrics.density
        this.tileSize = TILE_SIZE_DP * density
        this.strokeWidth = STROKE_WIDTH_DP * density
    }

    override fun getTile(x: Int, y: Int, zoom: Int): Tile {
        val bitmap = Bitmap.createBitmap(tileSize.toInt(),
                tileSize.toInt(), Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        if (isActive) {
            pathRenderer!!.drawPath(canvas, x, y, zoom)
        }

        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream)
        val bitmapData = stream.toByteArray()

        return Tile(tileSize.toInt(), tileSize.toInt(), bitmapData)
    }

    fun start(summaryField: ObservableField<Summary>, weightTypeField: ObservableField<WeightType>, googleMap: GoogleMap) {
        this.summaryField = summaryField
        this.weightTypeField = weightTypeField

        val options = TileOverlayOptions()
        options.tileProvider(this)
        options.fadeIn(true)
        titleOverLay = googleMap.addTileOverlay(options)

        summaryField.addOnPropertyChangedCallback(modelCallback)
        weightTypeField.addOnPropertyChangedCallback(modelCallback)
        val summary = summaryField.get()
        val weightType = weightTypeField.get() ?: WeightType.None
        if (summary != null) {
            waypointsDidChange(summary, weightType)
        }
    }

    fun stop() {
        summaryField.removeOnPropertyChangedCallback(modelCallback)
        weightTypeField.removeOnPropertyChangedCallback(modelCallback)
        titleOverLay.remove()
        cancel()
    }

    private fun waypointsDidChange(summary: Summary, weightType: WeightType) {
        launch {
            throttle(throttleCounter) {
                yield()
                pathRenderer = when (weightType) {
                    WeightType.None -> PathRenderer(tileSize, strokeWidth, summary, null)
                    WeightType.Speed -> {
                        val (min, max) = summary.deltas.flatten().standardRange {
                            val speed = it.speed()
                            if (speed >= 0f) speed else 0f
                        }
                        PathRenderer(tileSize, strokeWidth, summary) { summary, segment, range ->
                            val waypoints = summary.deltas[segment].subList(range.first, range.second + 1)
                            val speed = waypoints.speed()
                            val weight = ((speed - min) / (max - min))
                            if (weight.isNaN()) {
                                Timber.w("Found altitude $speed with weight is $weight given $min...$max for $waypoints")
                            }
                            weight
                        }
                    }
                    WeightType.Altitude -> {
                        val (min, max) = summary.waypoints.flatten().standardRange {
                            val altitude = it.altitude
                            altitude.toFloat()
                        }
                        PathRenderer(tileSize, strokeWidth, summary) { summary, segment, range ->
                            val waypoints = summary.waypoints[segment].subList(range.first, range.second + 1)
                            val altitude = waypoints.height()
                            val weight = ((altitude - min) / (max - min))
                            if (weight.isNaN()) {
                                Timber.w("Found altitude $altitude with weight is $weight given $min...$max for $waypoints")
                            }
                            weight
                        }
                    }
                }
                launch(Dispatchers.Main) {
                    titleOverLay.clearTileCache()
                }
            }
        }
    }
}

private fun List<Waypoint>.height(): Float = (sumByDouble { it.altitude } / this.size).toFloat()

