/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.map

import android.net.Uri
import android.provider.BaseColumns
import androidx.annotation.WorkerThread
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import nl.renedegroot.android.gpstracker.ng.base.BaseConfiguration
import nl.renedegroot.android.gpstracker.ng.base.common.controllers.content.ContentController
import nl.renedegroot.android.gpstracker.ng.base.common.onMainThread
import nl.renedegroot.android.gpstracker.ng.base.dagger.DiskIO
import nl.renedegroot.android.gpstracker.ng.base.location.LocationFactory
import nl.renedegroot.android.gpstracker.ng.features.FeatureConfiguration
import nl.renedegroot.android.gpstracker.ng.features.map.rendering.StartEndMarkerProvider
import nl.renedegroot.android.gpstracker.ng.features.map.rendering.TrackRenderProviderFactory
import nl.renedegroot.android.gpstracker.ng.features.map.rendering.TrackTileProvider
import nl.renedegroot.android.gpstracker.ng.features.map.util.onMap
import nl.renedegroot.android.gpstracker.ng.features.model.Preferences
import nl.renedegroot.android.gpstracker.ng.features.model.not
import nl.renedegroot.android.gpstracker.ng.features.model.toggle
import nl.renedegroot.android.gpstracker.ng.features.model.valueOrFalse
import nl.renedegroot.android.gpstracker.ng.features.summary.SummaryManager
import nl.renedegroot.android.gpstracker.ng.features.util.AbstractSelectedTrackPresenter
import nl.renedegroot.android.gpstracker.ng.features.util.LoggingStateController
import nl.renedegroot.android.gpstracker.ng.features.util.LoggingStateListener
import nl.renedegroot.android.gpstracker.ng.features.util.isLogging
import nl.renedegroot.android.gpstracker.service.util.asLatLng
import nl.renedegroot.android.gpstracker.service.util.trackUri
import nl.renedegroot.android.gpstracker.service.util.tracksUri
import nl.renedegroot.android.gpstracker.utils.contentprovider.getLong
import nl.renedegroot.android.gpstracker.utils.contentprovider.runQuery
import nl.renedegroot.android.opengpstrack.ng.note.NoteMarker
import nl.renedegroot.android.opengpstrack.ng.note.NoteMarkerController
import java.util.concurrent.Executor
import javax.inject.Inject

class TrackMapPresenter(
        val navigation: MutableLiveData<MapNavigation> = MutableLiveData()
) : AbstractSelectedTrackPresenter(), ContentController.Listener, LoggingStateListener {

    @Inject
    lateinit var trackRenderProviderFactory: TrackRenderProviderFactory
    @Inject
    lateinit var locationFactory: LocationFactory
    @Inject
    lateinit var loggingStateController: LoggingStateController
    @Inject
    lateinit var preferences: Preferences
    @field:DiskIO
    @Inject
    lateinit var executor: Executor
    @Inject
    lateinit var summaryManager: SummaryManager
    @Inject
    lateinit var noteMarkerController: NoteMarkerController

    private var tileProvider: TrackTileProvider? = null
    private var startEndMarkerProvider: StartEndMarkerProvider? = null
    internal val viewModel = TrackMapViewModel()
    private val wakelockPreferenceObserver = Observer<Boolean> {
        viewModel.willLock.set(it ?: false)
        updateLock()
    }
    private val satellitePreferenceObserver = Observer<Boolean> {
        viewModel.showSatellite.set(it ?: false)
    }
    private val weightObserver = Observer<WeightType> {
        viewModel.weightType.set(it ?: WeightType.Speed)
    }
    private val notesObserver = Observer<List<NoteMarker>> {
        viewModel.notes.set(it)
    }

    init {
        FeatureConfiguration.featureComponent.inject(this)
    }

    override fun onFirstStart() {
        super.onFirstStart()
        preferences.wakelockScreen.observeForever(wakelockPreferenceObserver)
        preferences.satellite.observeForever(satellitePreferenceObserver)
        preferences.weightType.observeForever(weightObserver)
        noteMarkerController.markers.observeForever(notesObserver)
        executor.execute { makeTrackSelection() }
    }

    override fun onStart() {
        super.onStart()
        loggingStateController.connect(this)
        summaryManager.start()

    }

    fun onShowMapView(mapView: MapView) {
        mapView.onMap { map ->
            tileProvider = trackRenderProviderFactory.createTrackTileProvider(mapView.context.resources)
            tileProvider?.start(viewModel.summary, viewModel.weightType, map)
            startEndMarkerProvider = trackRenderProviderFactory.createStartEndMarkerProvider(mapView.context.resources)
            startEndMarkerProvider?.start(viewModel.summary, map)
        }
    }

    override fun onStop() {
        super.onStop()
        loggingStateController.disconnect()
        tileProvider?.stop()
        tileProvider = null
        startEndMarkerProvider?.stop()
        startEndMarkerProvider = null
        summaryManager.stop()
    }

    override fun onCleared() {
        preferences.wakelockScreen.removeObserver(wakelockPreferenceObserver)
        preferences.satellite.removeObserver(satellitePreferenceObserver)
        preferences.weightType.removeObserver(weightObserver)
        noteMarkerController.markers.removeObserver(notesObserver)
        super.onCleared()
    }

    override fun onTrackUpdate(trackUri: Uri?, name: String) {
        viewModel.trackUri.set(trackUri)
        viewModel.name.set(name)
        if (trackUri != null) {
            startReadingTrack(trackUri)
        } else {
            viewModel.name.set(name)
            viewModel.summary.set(null)
            viewModel.completeBounds.set(null)
            viewModel.trackHead.set(null)
        }
    }

    //region Service connecting

    override fun didStartLogging(trackUri: Uri) = onMainThread {
        updateLock()
        trackSelection.selection.value = trackUri
    }


    override fun didPauseLogging(trackUri: Uri) = onMainThread {
        updateLock()
    }

    override fun didStopLogging() = onMainThread {
        updateLock()
    }

    //endregion

    //region View callbacks

    fun onClickMyLocation() {
        viewModel.trackHead.set(locationFactory.getLocationCoordinates())
        viewModel.completeBounds.set(null)
    }

    fun onSatelliteSelected() {
        preferences.satellite.not()
    }

    fun onScreenLockSelected() {
        preferences.wakelockScreen.not()
    }

    fun onToggleLineWeightType() {
        preferences.weightType.toggle()
    }

    fun onMarkLocation(latLng: LatLng) {
        if (loggingStateController.isLogging()) {
            loggingStateController.lastWaypoint?.let {
                navigation.value = MapNavigation.CreateWaypoint(it)
            }
        }
    }

    fun onMarkerClicked(marker: Marker): Boolean {
        (marker.tag as? Uri)?.let {
            navigation.value = MapNavigation.ShowWaypoint(it)
            return true
        }
        return false
    }

    /* Private */

    private fun updateLock() {
        val isLogging = loggingStateController.isLogging()
        val shouldLock = preferences.wakelockScreen.valueOrFalse()
        viewModel.isLocked.set(isLogging && shouldLock)
    }

    private fun startReadingTrack(trackUri: Uri) {
        noteMarkerController.startOnTrack(trackUri)
        summaryManager.collectSummaryInfo(trackUri) { summary ->
            viewModel.name.set(summary.name)
            viewModel.summary.set(summary)
            if (loggingStateController.isLogging()) {
                viewModel.completeBounds.set(null)
                viewModel.trackHead.set(summary.waypoints.lastOrNull()?.lastOrNull()?.asLatLng())
            } else {
                viewModel.trackHead.set(null)
                viewModel.completeBounds.set(summary.bounds)
            }
        }
    }

    @WorkerThread
    private fun makeTrackSelection() {
        val selectedTrack = trackSelection.selection.value
        if (selectedTrack == null || selectedTrack.lastPathSegment != "-1") {
            val lastTrack = tracksUri().runQuery(BaseConfiguration.appComponent.contentResolver()) { it.moveToLast(); it.getLong(BaseColumns._ID) }
            if (lastTrack != null) {
                val lastTrackUri = trackUri(lastTrack)
                trackSelection.selection.postValue(lastTrackUri)
            } else {
                viewModel.trackHead.set(locationFactory.getLocationCoordinates())
            }
        }
    }
}
