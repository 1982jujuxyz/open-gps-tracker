/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.features.trackedit

import android.content.Context
import androidx.appcompat.content.res.AppCompatResources
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import nl.renedegroot.android.opengpstrack.ng.features.R

class TrackTypeSpinnerAdapter(val context: Context, var trackTypes: List<TrackTypeDescriptions.TrackType>) : BaseAdapter() {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val viewHolder: TrackEditPresenter.ViewHolder
        val itemView: View
        if (convertView == null) {
            itemView = LayoutInflater.from(context).inflate(R.layout.row_track_type, parent, false)
            viewHolder = TrackEditPresenter.ViewHolder(
                    itemView.findViewById(R.id.row_track_type_image),
                    itemView.findViewById(R.id.row_track_type_text))
            itemView.tag = viewHolder
        } else {
            itemView = convertView
            viewHolder = convertView.tag as TrackEditPresenter.ViewHolder
        }
        val trackType = trackTypes[position]
        viewHolder.textView.text = context.getString(trackType.stringId)
        context.let { viewHolder.imageView.setImageDrawable(AppCompatResources.getDrawable(it, trackType.drawableId)) }

        return itemView
    }

    override fun getItem(position: Int): TrackTypeDescriptions.TrackType = trackTypes[position]

    override fun getItemId(position: Int): Long = trackTypes[position].drawableId.toLong()

    override fun getCount() = trackTypes.size
}
