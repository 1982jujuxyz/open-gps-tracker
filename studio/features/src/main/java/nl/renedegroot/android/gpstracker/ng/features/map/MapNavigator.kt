package nl.renedegroot.android.gpstracker.ng.features.map

import android.content.Context
import android.net.Uri
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import nl.renedegroot.android.opengpstrack.ng.note.newNoteFragmentInstance

private const val TAG_WAYPOINT_NOTE = "TAG_WAYPOINT_NOTE"

sealed class MapNavigation {
    data class CreateWaypoint(val waypoint: Uri) : MapNavigation()
    data class ShowWaypoint(val waypoint: Uri) : MapNavigation()
}

class MapNavigator(val context: Context, val manager: FragmentManager) : Observer<MapNavigation> {
    override fun onChanged(navigation: MapNavigation) =
            when (navigation) {
                is MapNavigation.CreateWaypoint -> createWaypoint(navigation.waypoint)
                is MapNavigation.ShowWaypoint -> showWaypoint(navigation.waypoint)
            }

    private fun createWaypoint(waypoint: Uri) {
        val dialog = newNoteFragmentInstance(waypoint, editable = true)
        dialog.show(manager, TAG_WAYPOINT_NOTE)
    }

    private fun showWaypoint(waypoint: Uri) {
        val dialog = newNoteFragmentInstance(waypoint, editable = false)
        dialog.show(manager, TAG_WAYPOINT_NOTE)
    }
}
