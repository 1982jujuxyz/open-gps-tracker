/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.features.graphs.dataproviders

import android.content.Context
import nl.renedegroot.android.gpstracker.ng.features.FeatureConfiguration
import nl.renedegroot.android.gpstracker.ng.features.graphs.GraphSpeedConverter
import nl.renedegroot.android.gpstracker.v2.sharedwear.R
import nl.renedegroot.android.gpstracker.v2.sharedwear.util.getAsFloat
import javax.inject.Inject

class SpeedRangePicker(private val inverseSpeed: Boolean) {

    @Inject
    lateinit var graphSpeedConverter: GraphSpeedConverter

    init {
        FeatureConfiguration.featureComponent.inject(this)
    }

    fun prettyMinYValue(context: Context, yValue: Float): Float =
            if (inverseSpeed) {
                lowerBoundForInverseValue(yValue)
            } else {
                lowerBoundForValue(context, yValue)
            }

    fun prettyMaxYValue(context: Context, yValue: Float) =
            if (inverseSpeed) {
                upperBoundForInverseValue(yValue)
            } else {
                upperBoundForValue(context, yValue)
            }

    private fun lowerBoundForValue(context: Context, yValue: Float): Float {
        val conversion = context.resources.getAsFloat(nl.renedegroot.android.gpstracker.v2.sharedwear.R.string.mps_to_speed)
        val speed = yValue * conversion
        return (20 * (speed.toInt() / 20)) / conversion
    }

    private fun upperBoundForValue(context: Context, yValue: Float): Float {
        val conversion = context.resources.getAsFloat(R.string.mps_to_speed)
        val speed = yValue * conversion
        return (20 * (1 + speed.toInt() / 20)) / conversion
    }

    private fun lowerBoundForInverseValue(yValue: Float): Float {
        return yValue.toInt().toFloat()
    }

    private fun upperBoundForInverseValue(yValue: Float) =
            (1 + yValue.toInt()).toFloat()
}
