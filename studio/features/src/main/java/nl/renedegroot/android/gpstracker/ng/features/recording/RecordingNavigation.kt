/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.recording

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import nl.renedegroot.android.opengpstrack.ng.features.R
import javax.inject.Inject

const val GPS_STATUS_PACKAGE_NAME = "com.eclipsim.gpsstatus2"

class RecordingNavigation @Inject constructor(private val packageManager: PackageManager) {

    fun observe(lifecycle: LifecycleOwner, context: Context, liveData: LiveData<Navigation>) {
        liveData.observe(lifecycle, Observer {
            when (it) {
                is Navigation.GpsStatusAppOpen -> openExternalGpsStatusApp(context)
                is Navigation.GpsStatusAppInstallHint -> showInstallHintForGpsStatusApp(context)
            }
        })
    }

    private fun openExternalGpsStatusApp(context: Context) {
        packageManager.getLaunchIntentForPackage(GPS_STATUS_PACKAGE_NAME)?.let { intent ->
            intent.addCategory(Intent.CATEGORY_LAUNCHER)
            context.startActivity(intent)
        }
    }

    private fun showInstallHintForGpsStatusApp(context: Context) {
        AlertDialog.Builder(context)
                .setTitle(R.string.fragment_recording_gpsstatus_title)
                .setMessage(R.string.fragment_recording_gpsstatus_body)
                .setNegativeButton(android.R.string.cancel) { dialog, _ ->
                    dialog.dismiss()
                }
                .setPositiveButton(R.string.permission_button_install) { _, _ ->
                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$GPS_STATUS_PACKAGE_NAME"))
                    context.startActivity(intent)
                }
                .show()
    }
}
