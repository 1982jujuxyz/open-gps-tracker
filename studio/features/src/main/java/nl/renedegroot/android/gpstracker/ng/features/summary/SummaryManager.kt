/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.summary

import android.content.Context
import android.net.Uri
import nl.renedegroot.android.gpstracker.ng.features.FeatureConfiguration
import nl.renedegroot.android.gpstracker.ng.features.dagger.FeatureScope
import nl.renedegroot.android.gpstracker.service.integration.ContentConstants.Waypoints.WAYPOINTS
import nl.renedegroot.android.gpstracker.utils.concurrent.BackgroundThreadFactory
import nl.renedegroot.android.gpstracker.utils.contentprovider.append
import nl.renedegroot.android.gpstracker.utils.contentprovider.count
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.atomic.AtomicInteger
import javax.inject.Inject

/**
 * Helps in the retrieval, create and keeping up to date of summary data
 */
@FeatureScope
class SummaryManager @Inject constructor(private val calculator: SummaryCalculator, private val context: Context) {

    private var executor = Executors.newFixedThreadPool(numberOfThreads(), BackgroundThreadFactory("SummaryManager"))
    private val summaryCache = ConcurrentHashMap<Uri, Summary>()
    private var activeCount = AtomicInteger(0)

    init {
        FeatureConfiguration.featureComponent.inject(this)
    }

    fun start() {
        activeCount.addAndGet(1)
    }

    fun stop() {
        activeCount.addAndGet(-1)
    }

    /**
     * Collects summary data from the meta table.
     */
    fun collectSummaryInfo(trackUri: Uri,
                           callbackSummary: (Summary) -> Unit) {
        val executor = executor ?: return
        executor.submit {
            val cacheHit = summaryCache[trackUri]
            if (cacheHit != null) {
                val trackWaypointsUri = trackUri.append(WAYPOINTS)
                val trackCount = trackWaypointsUri.count(context.contentResolver)
                val cacheCount = cacheHit.count
                if (trackCount == cacheCount) {
                    callbackSummary(cacheHit)
                } else {
                    executeTrackCalculation(trackUri, callbackSummary)
                }
            } else {
                executeTrackCalculation(trackUri, callbackSummary)
            }
        }
    }

    private fun executeTrackCalculation(trackUri: Uri, callbackSummary: (Summary) -> Unit) {
        if (activeCount.get() > 0) {
            val summary = calculator.calculateSummary(trackUri)
            if (activeCount.get() > 0) {
                summaryCache[trackUri] = summary
                callbackSummary(summary)
            }
        }
    }

    private fun numberOfThreads() = Runtime.getRuntime().availableProcessors()

    fun invalidateCache(trackUri: Uri) {
        summaryCache.remove(trackUri)
    }
}
