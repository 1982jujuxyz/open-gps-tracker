/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.recording

import androidx.databinding.BindingAdapter
import androidx.appcompat.content.res.AppCompatResources
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import nl.renedegroot.android.gpstracker.ng.features.FeatureConfiguration
import nl.renedegroot.android.opengpstrack.ng.features.R

@BindingAdapter("isRecording")
fun setRecording(container: ViewGroup, isRecording: Boolean?) {
    if (isRecording == true) {
        container.visibility = View.VISIBLE
        container.animate().translationY(0f).start()
    } else {
        container.animate().translationY((-container.height).toFloat()).withEndAction { container.visibility = View.GONE }.start()
    }
}

@BindingAdapter("gps_signal_quality")
fun setGpsSignalQuality(imageView: ImageView, quality: Int?) {
    when (quality) {
        0 -> imageView.setImageDrawable(AppCompatResources.getDrawable(imageView.context, R.drawable.ic_satellite_none))
        1 -> imageView.setImageDrawable(AppCompatResources.getDrawable(imageView.context, R.drawable.ic_satellite_low))
        2 -> imageView.setImageDrawable(AppCompatResources.getDrawable(imageView.context, R.drawable.ic_satellite_medium))
        3 -> imageView.setImageDrawable(AppCompatResources.getDrawable(imageView.context, R.drawable.ic_satellite_high))
        4 -> imageView.setImageDrawable(AppCompatResources.getDrawable(imageView.context, R.drawable.ic_satellite_full))
        else -> imageView.setImageDrawable(AppCompatResources.getDrawable(imageView.context, R.drawable.ic_satellite_none))
    }
}

@BindingAdapter("android:text")
fun setSummary(textView: TextView, summary: SummaryText?) {
    if (summary == null) {
        textView.text = ""
    } else {
        val statisticsFormatter = FeatureConfiguration.featureComponent.statisticsFormatter()
        val context = textView.context
        val speed = statisticsFormatter.convertMeterPerSecondsToSpeed(context, summary.meterPerSecond, summary.isRunners)
        val distance = statisticsFormatter.convertMetersToDistance(context, summary.meters)
        val duration = statisticsFormatter.convertSpanDescriptiveDuration(context, summary.msDuration)
        textView.text = context.getString(summary.string, distance, duration, speed)
    }
}

@BindingAdapter("android:text")
fun setIntegerText(textView: TextView, textId: Int?) {
    if (textId == null) {
        textView.text = ""
    } else {
        textView.text = textView.resources.getText(textId)
    }
}
