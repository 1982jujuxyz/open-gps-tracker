/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.utils.test

import java.io.Closeable

/**
 * Based on the description by Vladimir Bezhenar
 * https://discuss.kotlinlang.org/t/is-there-standard-way-to-use-multiple-resources/2613
 */
class Resources : Closeable {
    private val resources = mutableListOf<Closeable>()

    fun <T : Closeable> T.use(): T {
        resources += this
        return this
    }

    override fun close() {
        resources.reversed().forEach { resource ->
            try {
                resource.close()
            } catch (closeException: Exception) {
                //ignore
            }
        }
    }
}

inline fun <T> withResources(block: Resources.() -> T): T = Resources().use(block)
