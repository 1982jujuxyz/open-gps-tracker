/*
 * Open GPS Tracker
 * Copyright (C) 2019 René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses>.
 */
package nl.renedegroot.android.gpstracker.utils.permissions

import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import nl.renedegroot.android.test.utils.BuildConfig.controlPermission
import nl.renedegroot.android.test.utils.BuildConfig.tracksPermission
import nl.renedegroot.android.test.utils.R
import timber.log.Timber

private const val REQUEST_CODE_PERMISSION_REQUESTER = 10001

/**
 * Asks for (Open GPS tracker) permissions
 */
class PermissionRequester(private val permissionChecker: PermissionChecker) {

    private val explanation: Int = R.string.permission_explain_need_control
    private val grants = MutableLiveData<List<String>>()
    private val isShowingDialog: Boolean
        get() = arrayOf(permissionDialog, installDialog, missingDialog)
                .fold(false) { acc, dialog -> acc || dialog != null }
    private var permissionDialog: AlertDialog? = null
    private var installDialog: AlertDialog? = null
    private var missingDialog: AlertDialog? = null
    private var request: Array<String> = emptyArray()

    fun start(
            fragment: Fragment,
            permissions: List<String> = listOf(tracksPermission, controlPermission, ACCESS_FINE_LOCATION),
            onHadPermissions: () -> Unit
    ) =
            if (permissions.all { hasPermission(fragment.requireContext(), it) }) {
                Timber.d("For start in $fragment validated all permission $permissions")
                onHadPermissions()
            } else {
                Timber.d("For $fragment missing some of $permissions")
                grants.observe(fragment.viewLifecycleOwner, object : Observer<List<String>> {
                    override fun onChanged(ignored: List<String>?) {
                        if (permissions.all { hasPermission(fragment.requireContext(), it) }) {
                            grants.removeObserver(this)
                            Timber.d("For $fragment noticed incoming $permissions")
                            onHadPermissions()
                        }
                    }
                })
                checkAccess(fragment, permissions)
            }


    fun stop(
            fragment: Fragment,
            permissions: List<String> = listOf(tracksPermission, controlPermission, ACCESS_FINE_LOCATION),
            onHadPermissions: () -> Unit
    ) {
        installDialog?.dismiss()
        installDialog = null
        permissionDialog?.dismiss()
        permissionDialog = null
        grants.removeObservers(fragment)
        if (permissions.all { hasPermission(fragment.requireContext(), it) }) {
            Timber.d("For stop in $fragment validated all permission $permissions")
            onHadPermissions()
        }
    }

    private fun didReceivePermissions(permissions: List<String>) {
        grants.postValue(permissions)
    }

    private fun checkAccess(fragment: Fragment, permissions: List<String>) {
        val activity = fragment.requireActivity()

        val received = permissions.filter { hasPermission(activity, it) }
        val canAsk = permissions.filter { canAsk(activity, it) }
        val shouldExplain = permissions.filter { shouldExplain(activity, it) }

        didReceivePermissions(received)

        if (canAsk.isNotEmpty()) {
            showRequest(fragment, canAsk.toTypedArray())
        } else if (shouldExplain.isNotEmpty()) {
            val request = DialogInterface.OnClickListener { _, _ -> showRequest(fragment, shouldExplain.toTypedArray()) }
            val cancel = DialogInterface.OnClickListener { _, _ -> cancel() }
            showRationale(activity, request, cancel)
        }
    }

    fun onRequestPermissionsResult(fragment: Fragment, requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        val activity = fragment.activity ?: throw IllegalStateException("Require attached fragment")
        if (requestCode == REQUEST_CODE_PERMISSION_REQUESTER) {
            synchronized(request) {
                request = emptyArray()
                val grants = grantResults.indices
                        .filter { grantResults[it] == PackageManager.PERMISSION_GRANTED }
                        .map { permissions[it] }
                if (permissions.all { grants.contains(it) }) {
                    didReceivePermissions(permissions.filter { hasPermission(activity, it) })
                } else {
                    val missing = grantResults.indices
                            .filter { grantResults[it] != PackageManager.PERMISSION_GRANTED }
                            .map { permissions[it] }
                    val ok = DialogInterface.OnClickListener { _, _ -> checkAccess(fragment, missing) }
                    showMissing(activity, missing, ok)
                }
            }
        }
    }

    private fun cancel() {
        installDialog = null
        permissionDialog = null
    }

    private fun showRationale(context: Context, okListener: DialogInterface.OnClickListener, cancel: DialogInterface.OnClickListener) {
        if (!isShowingDialog) {
            permissionDialog = AlertDialog.Builder(context)
                    .setMessage(explanation)
                    .setNegativeButton(android.R.string.cancel, cancel)
                    .setPositiveButton(android.R.string.ok, okListener)
                    .show()
        }
    }

    private fun showRequest(fragment: Fragment, permissions: Array<String>) {
        synchronized(request) {
            permissionDialog?.dismiss()
            permissionDialog = null
            if (request.isEmpty() && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                request = permissions
                fragment.requestPermissions(request, REQUEST_CODE_PERMISSION_REQUESTER)
            }
        }
    }

    private fun showMissing(context: Context, missing: List<String>, ok: DialogInterface.OnClickListener) {
        if (!isShowingDialog) {
            val permissions = missing.joinToString(prefix = "Missing required permission:\n", separator = "\n") { it.removePrefix("android.permission.") }
            missingDialog = AlertDialog.Builder(context)
                    .setMessage(permissions)
                    .setNegativeButton(android.R.string.cancel, null)
                    .setPositiveButton(android.R.string.ok, ok)
                    .setOnDismissListener { missingDialog = null }
                    .show()
        }
    }

    private fun shouldExplain(activity: Activity, permission: String): Boolean {
        return permissionChecker.shouldShowRequestPermissionRationale(activity, permission)
    }

    private fun canAsk(activity: Activity, permission: String): Boolean {
        return !hasPermission(activity, permission) && !permissionChecker.shouldShowRequestPermissionRationale(activity, permission)
    }

    private fun hasPermission(context: Context, permission: String): Boolean {
        return permissionChecker.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED
    }
}
