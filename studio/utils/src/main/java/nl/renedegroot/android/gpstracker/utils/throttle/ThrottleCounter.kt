/*
 * Open GPS Tracker
 * Copyright (C) 2019 René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses>.
 */

package nl.renedegroot.android.gpstracker.utils.throttle

import android.os.SystemClock
import kotlinx.coroutines.delay
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

class ThrottleCounter(private val delayMilliseconds: Int = 1000) {
    private var function: (suspend () -> Unit)? = null
    private var lastExecution = 0L
    private var lock = ReentrantLock()

    internal fun timeToNextExecution(function: suspend () -> Unit): Long =
            lock.withLock {
                this.function = function
                val currentTime = SystemClock.elapsedRealtime()
                lastExecution + delayMilliseconds - currentTime
            }

    internal suspend fun invoke() {
        val function = lock.withLock {
            val ref = function
            function = null
            lastExecution = SystemClock.elapsedRealtime()
            ref
        }
        function?.let {
            it.invoke()
        }
    }
}

suspend fun throttle(
        throttleCounter: ThrottleCounter,
        function: suspend () -> Unit
) {
    val timeToDelay = throttleCounter.timeToNextExecution(function)
    if (timeToDelay <= 0) {
        throttleCounter.invoke()
    } else {
        delay(timeToDelay)
        throttleCounter.invoke()
    }
}
