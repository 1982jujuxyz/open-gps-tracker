/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.base.common.controllers.gpsstatus

import android.annotation.SuppressLint
import android.content.Context
import android.location.GpsSatellite
import android.location.GpsStatus
import android.location.LocationManager
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.any
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnit

@SuppressLint("MissingPermission")
class GpsStatusControllerImplTest {

    @get:Rule
    var mockitoRule = MockitoJUnit.rule()
    lateinit var sut: GpsStatusControllerImpl
    @Mock
    lateinit var context: Context
    @Mock
    lateinit var listener: GpsStatusController.Listener
    @Mock
    lateinit var locationManager: LocationManager
    @Captor
    lateinit var callbackCapture: ArgumentCaptor<GpsStatus.Listener>

    @Before
    fun setUp() {
        `when`(context.getSystemService(Context.LOCATION_SERVICE)).thenReturn(locationManager)
        sut = GpsStatusControllerImpl(context)
    }

    @Test
    fun startUpdates() {
        // Act
        sut.startUpdates(listener)
        // Assert
        verify(locationManager).addGpsStatusListener(any())
    }

    @Test
    fun stopUpdates() {
        // Arrange
        sut.startUpdates(listener)
        // Act
        sut.stopUpdates()
        // Assert
        verify(locationManager).removeGpsStatusListener(any())
    }

    @Test
    fun start() {
        // Arrange
        sut.startUpdates(listener)
        verify(locationManager).addGpsStatusListener(callbackCapture.capture())
        val callback = callbackCapture.value
        // Act
        callback.onGpsStatusChanged(GpsStatus.GPS_EVENT_STARTED)
        // Assert
        verify(listener).onStartListening()
    }

    @Test
    fun stop() {
        // Arrange
        sut.startUpdates(listener)
        verify(locationManager).addGpsStatusListener(callbackCapture.capture())
        val callback = callbackCapture.value
        // Act
        callback.onGpsStatusChanged(GpsStatus.GPS_EVENT_STOPPED)
        // Assert
        verify(listener).onStopListening()
    }

    @Test
    fun firstFix() {
        // Arrange
        sut.startUpdates(listener)
        verify(locationManager).addGpsStatusListener(callbackCapture.capture())
        val callback = callbackCapture.value
        // Act
        callback.onGpsStatusChanged(GpsStatus.GPS_EVENT_FIRST_FIX)
        // Assert
        verify(listener).onFirstFix()
    }

    @Test
    fun statellites() {
        // Arrange
        sut.startUpdates(listener)
        verify(locationManager).addGpsStatusListener(callbackCapture.capture())
        val callback = callbackCapture.value
        val status = mock(GpsStatus::class.java)
        `when`(locationManager.getGpsStatus(null)).thenReturn(status)
        `when`(status.maxSatellites).thenReturn(3)
        `when`(status.satellites).thenReturn(listOf<GpsSatellite?>(null, null))
        // Act
        callback.onGpsStatusChanged(GpsStatus.GPS_EVENT_SATELLITE_STATUS)
        // Assert
        verify(listener).onChange(2, 3)
    }
}
