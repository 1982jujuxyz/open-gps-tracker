/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.base.common.controllers.gpsstatus

import android.annotation.SuppressLint
import android.content.Context
import android.location.GnssStatus
import android.location.LocationManager
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnit

@SuppressLint("NewApi")
class GnnsStatusControllerImplTest {

    @get:Rule
    var mockitoRule = MockitoJUnit.rule()
    @Mock
    lateinit var context: Context
    @Mock
    lateinit var locationManager: LocationManager
    @Mock
    lateinit var listener: GpsStatusController.Listener
    @Captor
    lateinit var callbackCapture: ArgumentCaptor<GnssStatus.Callback>
    lateinit var sut: GnnsStatusControllerImpl

    @Before
    fun setUp() {
        `when`(context.getSystemService(Context.LOCATION_SERVICE)).thenReturn(locationManager)
        sut = GnnsStatusControllerImpl(context)
    }

    @Test
    fun startUpdates() {
        // Act
        sut.startUpdates(listener)
        // Assert
        verify(locationManager).registerGnssStatusCallback(any())
    }

    @Test
    fun stopUpdates() {
        // Arrange
        sut.startUpdates(listener)
        // Act
        sut.stopUpdates()
        // Assert
        verify(locationManager).unregisterGnssStatusCallback(any())
    }

    @Test
    fun start() {
        // Arrange
        sut.startUpdates(listener)
        verify(locationManager).registerGnssStatusCallback(callbackCapture.capture())
        val callback = callbackCapture.value
        // Act
        callback.onStarted()
        // Assert
        verify(listener).onStartListening()
    }

    @Test
    fun stop() {
        // Arrange
        sut.startUpdates(listener)
        verify(locationManager).registerGnssStatusCallback(callbackCapture.capture())
        val callback = callbackCapture.value
        // Act
        callback.onStopped()
        // Assert
        verify(listener).onStopListening()
    }

    @Test
    fun firstFix() {
        // Arrange
        sut.startUpdates(listener)
        verify(locationManager).registerGnssStatusCallback(callbackCapture.capture())
        val callback = callbackCapture.value
        // Act
        callback.onFirstFix(123)
        // Assert
        verify(listener).onFirstFix()
    }

    @Test
    fun statellites() {
        // Arrange
        sut.startUpdates(listener)
        verify(locationManager).registerGnssStatusCallback(callbackCapture.capture())
        val callback = callbackCapture.value
        val status = mock(GnssStatus::class.java)
        `when`(status.satelliteCount).thenReturn(3)
        `when`(status.usedInFix(0)).thenReturn(true)
        `when`(status.usedInFix(2)).thenReturn(true)
        // Act
        callback.onSatelliteStatusChanged(status)
        // Assert
        verify(listener).onChange(2, 3)
    }
}
