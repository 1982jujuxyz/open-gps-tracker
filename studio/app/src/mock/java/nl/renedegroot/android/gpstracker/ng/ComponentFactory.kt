/*
 * Open GPS Tracker
 * Copyright (C) 2019 René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses>.
 */

package nl.renedegroot.android.gpstracker.ng

import android.app.Application
import android.content.Context
import nl.renedegroot.android.gpstracker.ng.base.BaseConfiguration
import nl.renedegroot.android.gpstracker.ng.base.dagger.AppComponent
import nl.renedegroot.android.gpstracker.ng.base.dagger.AppModule
import nl.renedegroot.android.gpstracker.ng.dagger.DaggerMockAppComponent
import nl.renedegroot.android.gpstracker.ng.dagger.MockBaseSystemModule
import nl.renedegroot.android.gpstracker.ng.features.dagger.DaggerFeatureComponent
import nl.renedegroot.android.gpstracker.ng.features.dagger.FeatureComponent
import nl.renedegroot.android.gpstracker.ng.features.dagger.FeatureModule
import nl.renedegroot.android.gpstracker.ng.features.dagger.VersionInfoModule
import nl.renedegroot.android.gpstracker.v2.BuildConfig
import kotlin.math.min

class ComponentFactory {

    fun createAppComponent(application: Application): AppComponent =
            DaggerMockAppComponent.builder()
                    .appModule(AppModule(application))
                    .mockBaseSystemModule(MockBaseSystemModule())
                    .build()

    fun createFeatureComponent(context: Context): FeatureComponent =
            DaggerFeatureComponent.builder()
                    .appComponent(BaseConfiguration.appComponent)
                    .versionInfoModule(VersionInfoModule(version(), gitHash(), buildNumber()))
                    .featureModule(FeatureModule(context))
                    .build()


    private fun version() = BuildConfig.VERSION_NAME

    private fun buildNumber() = BuildConfig.BUILD_NUMBER.toString()

    private fun gitHash() = BuildConfig.GIT_COMMIT.take(min(7, BuildConfig.GIT_COMMIT.length))
}
