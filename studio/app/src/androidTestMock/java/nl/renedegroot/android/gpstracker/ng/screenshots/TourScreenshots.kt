/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.screenshots

import androidx.test.rule.ActivityTestRule
import androidx.test.rule.GrantPermissionRule
import nl.renedegroot.android.gpstracker.ng.features.track.TrackActivity
import nl.renedegroot.android.gpstracker.ng.robots.*
import org.junit.*


class TourScreenshots {

    @get:Rule
    var activityRule = ActivityTestRule<TrackActivity>(TrackActivity::class.java)
    @get:Rule
    var runtimePermissionRule = GrantPermissionRule.grant(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)


    private lateinit var trackRobot: TrackRobot
    private lateinit var aboutRobot: AboutRobot
    private lateinit var trackListRobot: TrackListRobot
    private lateinit var graphRobot: GraphRobot

    companion object {
        @BeforeClass
        @JvmStatic
        fun setupOnce() {
            Robot.resetScreenShots()
        }
    }

    @Before
    fun setUp() {
        trackRobot = TrackRobot(activityRule.activity)
        aboutRobot = AboutRobot(activityRule.activity)
        trackListRobot = TrackListRobot()
        graphRobot = GraphRobot()

    }

    @After
    fun tearDown() {
        trackRobot.stop()
        aboutRobot.stop()
    }

    @Test
    fun recordTrack() {
        trackRobot
                .start()
                .startRecording()
                .sleep(10).takeScreenShot()
                .pauseRecording().takeScreenShot()
                .resumeRecording().takeScreenShot()
                .sleep(10)
                .stopRecording().takeScreenShot()
                .stop()
    }

    @Test
    fun editTrack() {
        trackRobot
                .start()
                .takeScreenShot()
                .editTrack().takeScreenShot()
                .openTrackTypeSpinner().takeScreenShot()
                .selectWalking()
                .ok()
                .stop()
    }

    @Test
    fun trackList() {
        trackRobot
                .start()
                .openTrackList().takeScreenShot()
        trackListRobot
                .openRowContextMenu(0).takeScreenShot()
                .share().takeScreenShot()
                .back()
                .openRowContextMenu(0)
                .edit().takeScreenShot()
                .cancelEdit()
                .openRowContextMenu(0)
                .delete().takeScreenShot()
                .cancelDelete()
        trackRobot
                .stop()
    }

    @Test
    fun about() {
        trackRobot
                .start()
                .openAbout().takeScreenShot()
        aboutRobot
                .start()
                .ok()
        trackRobot
                .stop()
    }

    @Test
    fun graph() {
        trackRobot
                .start()
                .openGraph()
        graphRobot
                .takeScreenShot()
                .back()
        trackRobot
                .stop()

    }
}
