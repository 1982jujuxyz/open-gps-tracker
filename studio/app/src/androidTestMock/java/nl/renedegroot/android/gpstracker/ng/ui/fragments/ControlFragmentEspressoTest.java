/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.ui.fragments;

import android.Manifest;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import androidx.test.espresso.IdlingRegistry;
import androidx.test.rule.GrantPermissionRule;
import nl.renedegroot.android.gpstracker.ng.features.control.ControlFragment;
import nl.renedegroot.android.gpstracker.ng.util.EspressoTestMatchers;
import nl.renedegroot.android.gpstracker.ng.util.FragmentTestRule;
import nl.renedegroot.android.gpstracker.service.mock.MockBroadcastSender;
import nl.renedegroot.android.gpstracker.service.mock.MockGpsListener;
import nl.renedegroot.android.gpstracker.v2.R;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

public class ControlFragmentEspressoTest {

    @Rule
    public GrantPermissionRule mRuntimePermissionRule = GrantPermissionRule.grant(Manifest.permission.WRITE_EXTERNAL_STORAGE);
    @Rule
    public FragmentTestRule<ControlFragment> wrapperFragment = new FragmentTestRule<>(ControlFragment.class);
    private ControlFragment sut;
    private MockGpsListener mockGpsListener;

    @Before
    public void setUp() {
        IdlingRegistry.getInstance().register(MockBroadcastSender.Espresso.getResource());
        sut = wrapperFragment.getFragment();
        mockGpsListener = new MockGpsListener(sut.requireContext());
        mockGpsListener.getGpsRecorder().setShouldScheduleWaypoints(false);
    }

    @After
    public void tearDown() {
        mockGpsListener.reset();
        mockGpsListener = null;
        sut = null;
        IdlingRegistry.getInstance().unregister(MockBroadcastSender.Espresso.getResource());
    }

    @Test
    public void testStartUp() {
        // Verify
        onView(withId(R.id.widget_control_right)).check(matches(isDisplayed()));
        onView(withId(R.id.widget_control_right)).check(matches(EspressoTestMatchers.withDrawable(R.drawable.ic_navigation_black_24dp)));
    }

    @Test
    public void testVisibleWhenStarted() {
        // Execute
        mockGpsListener.startGPSLogging("");
        // Verify
        onView(withId(R.id.widget_control_left)).check(matches(isDisplayed()));
        onView(withId(R.id.widget_control_right)).check(matches(isDisplayed()));
        onView(withId(R.id.widget_control_left)).check(matches(EspressoTestMatchers.withDrawable(R.drawable.ic_stop_black_24dp)));
        onView(withId(R.id.widget_control_right)).check(matches(EspressoTestMatchers.withDrawable(R.drawable.ic_pause_black_24dp)));
    }

    @Test
    public void testVisibleWhenPaused() {
        // Execute
        mockGpsListener.pauseGPSLogging();
        // Verify
        onView(withId(R.id.widget_control_left)).check(matches(isDisplayed()));
        onView(withId(R.id.widget_control_right)).check(matches(isDisplayed()));
        onView(withId(R.id.widget_control_left)).check(matches(EspressoTestMatchers.withDrawable(R.drawable.ic_stop_black_24dp)));
        onView(withId(R.id.widget_control_right)).check(matches(EspressoTestMatchers.withDrawable(R.drawable.ic_navigation_black_24dp)));
    }

    @Test
    public void testVisibleWhenResumed() {
        // Execute
        mockGpsListener.resumeGPSLogging();
        // Verify
        onView(withId(R.id.widget_control_left)).check(matches(isDisplayed()));
        onView(withId(R.id.widget_control_right)).check(matches(isDisplayed()));
        onView(withId(R.id.widget_control_left)).check(matches(EspressoTestMatchers.withDrawable(R.drawable.ic_stop_black_24dp)));
        onView(withId(R.id.widget_control_right)).check(matches(EspressoTestMatchers.withDrawable(R.drawable.ic_pause_black_24dp)));
    }

    @Test
    public void testVisibleWhenStopped() {
        // Execute
        mockGpsListener.stopGPSLogging();
        // Verify
        onView(withId(R.id.widget_control_right)).check(matches(isDisplayed()));
        onView(withId(R.id.widget_control_right)).check(matches(EspressoTestMatchers.withDrawable(R.drawable.ic_navigation_black_24dp)));
    }
}
