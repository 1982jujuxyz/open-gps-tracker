package nl.renedegroot.android.gpstracker.service.dagger

import android.content.Context
import dagger.Module
import dagger.Provides
import nl.renedegroot.android.gpstracker.ng.base.common.controllers.gpsstatus.GpsStatusController
import nl.renedegroot.android.gpstracker.ng.mock.MockGpsStatusController
import nl.renedegroot.android.gpstracker.service.mock.MockGpsListener
import nl.renedegroot.android.gpstracker.service.mock.MockStatePersistence
import nl.renedegroot.android.gpstracker.service.ng.internal.GpsListenerInterface
import nl.renedegroot.android.gpstracker.service.ng.internal.LoggerNotification
import nl.renedegroot.android.gpstracker.service.ng.internal.PowerManager
import nl.renedegroot.android.gpstracker.service.ng.internal.ServicePermissionChecker
import nl.renedegroot.android.gpstracker.service.ng.internal.StatePersistenceInterface

@Module
class MockSystemModule(val context: Context) {

    @Provides
    internal fun gpsStatusControllerFactory(): GpsStatusController = MockGpsStatusController()

    @Provides
    internal fun mockGpsListener(
            notification: LoggerNotification,
            powerManager: PowerManager,
            permissionChecker: ServicePermissionChecker
    ): GpsListenerInterface =
            MockGpsListener(notification, powerManager, permissionChecker)

    @Provides
    internal fun statePersistence(): StatePersistenceInterface =
            MockStatePersistence()
}
