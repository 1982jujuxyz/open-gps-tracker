/*
 * Open GPS Tracker
 * Copyright (C) 2019 René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses>.
 */

package nl.renedegroot.android.gpstracker.service.ng.internal

import android.location.Location
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.CoreMatchers.nullValue
import org.junit.Assert
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertThat
import org.junit.Test

class LocationFilterTest {

    private val referenceLocation: Location = mock {
        whenever(it.latitude).thenReturn(37.422006)
        whenever(it.longitude).thenReturn(-122.084095)
        whenever(it.accuracy).thenReturn(10f)
        whenever(it.altitude).thenReturn(12.5)
        whenever(it.time).thenReturn(10_000_000L)
    }
    /**
     * Other side of the golfpark, about 1100 meters in one minute time of reference
     */
    private val closeBy: Location = mock {
        whenever(it.latitude).thenReturn(37.422006 + 0.0001)
        whenever(it.longitude).thenReturn(-122.084095)
        whenever(it.accuracy).thenReturn(10f)
        whenever(it.altitude).thenReturn(12.5)
        whenever(it.time).thenReturn(10_000_000L + 6_000L)
    }
    private val sut = LocationFilter()

    @Test
    fun testSimpleLocation() {
        val simpleLocation: Location = mock {
            whenever(it.latitude).thenReturn(37.422006)
            whenever(it.longitude).thenReturn(-122.084095)
            whenever(it.accuracy).thenReturn(10f)
        }

        val filtered = sut.filter(simpleLocation)

        assertThat(filtered, `is`(notNullValue()))
    }

    @Test
    fun testInaccurateLocation() {
        val inAccurate: Location = mock {
            whenever(it.latitude).thenReturn(37.422006)
            whenever(it.longitude).thenReturn(-122.084095)
            whenever(it.accuracy).thenReturn(50f)
        }

        val filtered = sut.filter(inAccurate)

        assertThat(filtered, `is`(nullValue()))
    }

    @Test
    fun testCloseLocation() {
        sut.filter(referenceLocation)

        val filtered = sut.filter(closeBy)

        assertNotNull("An acceptable fix", filtered)
    }

    @Test
    fun testDefaultLocation() {
        val zeroZeroLocation: Location = mock {
            whenever(it.latitude).thenReturn(0.0)
            whenever(it.longitude).thenReturn(0.0)
        }

        val filtered = sut.filter(zeroZeroLocation)

        assertThat(filtered, `is`(nullValue()))
    }

    @Test
    fun testBetterSomethingThenNothingAccurateLocation() {
        val first: Location = mock {
            whenever(it.latitude).thenReturn(37.422006)
            whenever(it.longitude).thenReturn(-122.084095)
            whenever(it.accuracy).thenReturn(150f)
            whenever(it.hasAccuracy()).thenReturn(true)
        }
        val second: Location = mock {
            whenever(it.latitude).thenReturn(37.422006)
            whenever(it.longitude).thenReturn(-122.084095)
            whenever(it.accuracy).thenReturn(100f)
            whenever(it.hasAccuracy()).thenReturn(true)
        }
        val third: Location = mock {
            whenever(it.latitude).thenReturn(37.422006)
            whenever(it.longitude).thenReturn(-122.084095)
            whenever(it.accuracy).thenReturn(125f)
            whenever(it.hasAccuracy()).thenReturn(true)
        }

        Assert.assertNull("An unacceptable fix", sut.filter(first))
        Assert.assertNull("An unacceptable fix", sut.filter(second))
        val last = sut.filter(third)
        assertNotNull("An acceptable fix", last)
        Assert.assertEquals("Best one was the second one", second, last)
        Assert.assertNull("An unacceptable fix", sut.filter(first))
    }
}
