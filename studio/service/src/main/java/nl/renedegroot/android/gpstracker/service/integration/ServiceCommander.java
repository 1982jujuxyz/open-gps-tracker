/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.service.integration;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;

import androidx.annotation.NonNull;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import nl.renedegroot.android.gpstracker.service.BuildConfig;
import nl.renedegroot.android.gpstracker.service.R;
import nl.renedegroot.android.gpstracker.service.ng.CommandFactory;
import nl.renedegroot.android.gpstracker.service.util.TrackUriExtensionKt;

import static nl.renedegroot.android.gpstracker.service.integration.ServiceConstants.Commands.CONFIG_FOREGROUND;

public class ServiceCommander {

    private final Context context;
    private final CommandFactory commandFactory;

    @Inject
    public ServiceCommander(Context context) {
        this.context = context;
        commandFactory = new CommandFactory(context);
    }

    @NonNull
    Intent createServiceIntent() {
        Intent intent = new Intent();
        intent.setComponent(new ComponentName(BuildConfig.applicationId, "nl.renedegroot.android.gpstracker.service.ng.GpsLoggerService"));
        return intent;
    }

    public boolean hasForInitialName(@NonNull Uri trackUri) {
        String name = TrackUriExtensionKt.readName(trackUri);

        return context.getString(R.string.initial_track_name).equals(name);
    }

    public void startGPSLogging() {
        startGPSLogging(context.getString(R.string.initial_track_name));
    }

    public void startGPSLogging(String trackName) {
        startService(commandFactory.createStartLoggerCommand(trackName), true);
    }

    public void pauseGPSLogging() {
        startService(commandFactory.createPauseLoggerCommand(), false);
    }

    public void resumeGPSLogging() {
        startService(commandFactory.createResumeLoggerCommand(), true);
    }

    public void stopGPSLogging() {
        startService(commandFactory.createStopLoggerCommand(), false);
    }

    private void startService(Intent intent, boolean foreground) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && foreground) {
            intent.putExtra(CONFIG_FOREGROUND, true);
            context.startForegroundService(intent);
        } else {
            intent.putExtra(CONFIG_FOREGROUND, false);
            context.startService(intent);
        }
    }
}
