/*
 * Open GPS Tracker
 * Copyright (C) 2019 René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses>.
 */

package nl.renedegroot.android.gpstracker.service.ng.internal

import android.annotation.SuppressLint
import android.location.Criteria
import android.location.Criteria.ACCURACY_FINE
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.location.LocationManager.GPS_PROVIDER
import android.os.Bundle
import android.os.HandlerThread
import nl.renedegroot.android.gpstracker.service.ng.CommandFactory

private const val FINE_DISTANCE = 5f
private const val FINE_INTERVAL = 1000L

internal class GpsListener(
        private val locationManager: LocationManager,
        private val locationFilter: LocationFilter,
        private val notification: LoggerNotification,
        private val powerManager: PowerManager,
        private val permissionChecker: ServicePermissionChecker
) : GpsListenerInterface {

    private var gpsLogger: GpsLogger? = null
    private val looper by lazy {
        val handlerThread = HandlerThread("LocationUpdatesLooper")
        handlerThread.start()
        handlerThread.looper
    }

    private val locationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            notification.statusByProvider(GpsStatus.ReceivedLocation(location))
            locationFilter.filter(location)?.let {
                gpsLogger?.logLocation(it)
            }
        }

        override fun onStatusChanged(provider: String, status: Int, extras: Bundle?) {
            //NOOP statusListener collects more relevant information
        }

        override fun onProviderEnabled(provider: String) {
            notification.unblockedByDisabledProvider()
        }

        override fun onProviderDisabled(provider: String) {
            notification.blockedByDisabledProvider()
        }
    }

    @SuppressLint("MissingPermission")
    override fun startListener(gpsLogger: GpsLogger): Boolean {
        this.gpsLogger = gpsLogger
        return permissionChecker.check {
            val criteria = Criteria().apply {
                accuracy = ACCURACY_FINE
            }
            locationManager.requestLocationUpdates(
                    FINE_INTERVAL,
                    FINE_DISTANCE,
                    criteria,
                    locationListener,
                    looper
            )
            powerManager.updateWakeLock(5 * FINE_INTERVAL)
        }
    }

    override fun stopListener() {
        powerManager.release()
        locationManager.removeUpdates(locationListener)
        notification.leaveForeground()
    }
}
