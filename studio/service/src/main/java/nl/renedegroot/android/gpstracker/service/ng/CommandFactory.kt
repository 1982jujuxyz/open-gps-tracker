/*
 * Open GPS Tracker
 * Copyright (C) 2019 René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses>.
 */

package nl.renedegroot.android.gpstracker.service.ng

import android.content.Context
import android.content.Intent
import androidx.core.os.bundleOf
import org.jetbrains.annotations.NotNull

internal const val EXTRA_COMMAND = "EXTRA_COMMAND"
internal const val EXTRA_NAME = "EXTRA_NAME"
internal const val COMMAND_START = "START"
internal const val COMMAND_PAUSE = "PAUSE"
internal const val COMMAND_RESUME = "RESUME"
internal const val COMMAND_STOP = "STOP"

class CommandFactory(val context: Context) {

    private fun serviceIntent() = Intent(context, GpsLoggerService::class.java)

    fun createStartLoggerCommand(trackName: @NotNull String): @NotNull Intent = serviceIntent().apply {
        putExtras(bundleOf(EXTRA_COMMAND to COMMAND_START, EXTRA_NAME to trackName))
    }

    fun createPauseLoggerCommand() = serviceIntent().apply {
        putExtras(bundleOf(EXTRA_COMMAND to COMMAND_PAUSE))
    }

    fun createResumeLoggerCommand() = serviceIntent().apply {
        putExtras(bundleOf(EXTRA_COMMAND to COMMAND_RESUME))
    }

    fun createStopLoggerCommand() = serviceIntent().apply {
        putExtras(bundleOf(EXTRA_COMMAND to COMMAND_STOP))
    }
}
