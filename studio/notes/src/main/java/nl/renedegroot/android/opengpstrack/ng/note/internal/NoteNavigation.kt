/*
 * Open GPS Tracker
 * Copyright (C) 2019 René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses>.
 */

package nl.renedegroot.android.opengpstrack.ng.note.internal

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import nl.renedegroot.android.gpstracker.utils.Consumable

internal class NoteNavigation(navigation: MutableLiveData<Consumable<Navigation>> = MutableLiveData()) {
    private val _navigation = navigation
    val navigation: LiveData<Consumable<Navigation>>
        get() = _navigation

    fun dismiss() {
        _navigation.postValue(Consumable(Navigation.Dismiss))
    }

    fun pickImage() {
        _navigation.postValue(Consumable(Navigation.PickImage))
    }
}

internal sealed class Navigation {
    internal object Dismiss: Navigation()
    internal object PickImage: Navigation()
}
